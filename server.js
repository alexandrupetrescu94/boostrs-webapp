const express = require('express');
const app = express();

app.use(express.static("www"));
app.set('port', (process.env.PORT || 9000));
app.listen(app.get('port'), function() {
	console.log("App started on port ", app.get('port'));
})
