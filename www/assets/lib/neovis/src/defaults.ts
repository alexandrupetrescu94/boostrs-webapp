var defaults = {

    neo4j: {
        initialQuery:   `MATCH (n:Occupation) RETURN n`,
        neo4jUri: "https://hobby-ldadcoiekhacgbkedkhlopal.dbs.graphenedb.com:24780/db/data/",
        neo4jUser: "expressapi",
        neo4jPassword: "b.b7l60l7o8XlW.8bxIHbp4bZ6P1ViN",
    },
    visjs: {
        interaction: {
            hover: true,
            hoverConnectedEdges: true,
            selectConnectedEdges: false,
    //        multiselect: true,
            multiselect: 'alwaysOn',
            zoomView: false,
            experimental: { }
        },
        physics: {
            barnesHut: {
                damping: 0.1
            }
        },
        nodes: {
            mass: 4,
            shape: 'neo',
            labelHighlightBold: false,
            widthConstraint: {
                maximum: 40
            },
            heightConstraint: {
                maximum: 40
            }
        },
        edges: {
            hoverWidth: 0,
            selectionWidth: 0,
            smooth: {
                type: 'continuous',
                roundness: 0.15
            },
            font: {
                size: 9,
                strokeWidth: 0,
                align: 'top'
            },
            color: {
                inherit: false
            },
            arrows: {
                to: {
                    enabled: true,
                    type: 'arrow',
                    scaleFactor: 0.5
                }
            }
        }

    }
}

export { defaults };