webpackJsonp([0],{

/***/ 210:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 210;

/***/ }),

/***/ 252:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 252;

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_AuthenticationService__ = __webpack_require__(465);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__list_list__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = (function () {
    function LoginPage(formBuilder, navCtrl, authenticationService, storage, alertCtrl) {
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.authenticationService = authenticationService;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.loginForm = this.formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
        });
        this.loginInProgress = false;
        // this.guestLogin();
    }
    LoginPage.prototype.guestLogin = function () {
        var _this = this;
        if (!this.loginInProgress) {
            this.authenticationService.post({ username: 'alexandrup', password: 'test' }).subscribe(function (response) { return _this.loginSuccess(response); }, function (error) { return _this.loginError(error); }, function () { });
            this.loginInProgress = true;
        }
    };
    LoginPage.prototype.signIn = function () {
        var _this = this;
        if (!this.loginInProgress) {
            this.authenticationService.post(this.loginForm.value).subscribe(function (response) { return _this.loginSuccess(response); }, function (error) { return _this.loginError(error); }, function () { });
            this.loginInProgress = true;
        }
    };
    LoginPage.prototype.loginSuccess = function (response) {
        var _this = this;
        this.loginInProgress = false;
        if (response.success) {
            localStorage.setItem('token', response.token.toString());
            this.storage.set('token', response.token);
            // this.navCtrl.setRoot(HomePage);
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__list_list__["a" /* ListPage */]);
        }
        else {
            var alert_1 = this.alertCtrl.create({
                "title": "Error",
                "subTitle": response.message,
                "buttons": [
                    {
                        text: 'Ok',
                        role: 'Ok',
                        handler: function () {
                            _this.loginForm.reset();
                        }
                    },
                ]
            });
            alert_1.present();
        }
    };
    LoginPage.prototype.loginError = function (error) {
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"E:\boostrs\ngAdmin\src\pages\login\login.html"*/'<ion-content class="gradient" no-bounce>\n\n    <ion-grid>\n\n        <div class="loginTitle"></div>\n\n        <ion-row>\n\n            <ion-col col-12 col-md-8 col-lg-6 col-xl-4 class="loginForm">\n\n                <h1 class="loginText">Admin Panel</h1>\n\n                <form [formGroup]="loginForm">\n\n                    <ion-item>\n\n                        <ion-label floating>Username</ion-label>\n\n                        <ion-input formControlName="username" type="text"></ion-input>\n\n                    </ion-item>\n\n                    <ion-item>\n\n                        <ion-label floating>Password</ion-label>\n\n                        <ion-input formControlName="password" type="password"></ion-input>\n\n                    </ion-item>\n\n                    <button ion-button color="orangeA" (click)="signIn()" [disabled]="!loginForm.valid">Sign In</button>\n\n                    \n\n                    <!-- <h2 class="line"><span>or</span></h2> -->\n\n\n\n                    <button ion-button color="dark" (click)="guestLogin()">Sign In as Guest</button>\n\n                </form>            \n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"E:\boostrs\ngAdmin\src\pages\login\login.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__services_AuthenticationService__["a" /* AuthenticationService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__services_AuthenticationService__["a" /* AuthenticationService */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OccupationAssociationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_OccupationsService__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__list_list__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OccupationAssociationsPage = (function () {
    function OccupationAssociationsPage(navCtrl, navParams, occupationsService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.occupationsService = occupationsService;
    }
    OccupationAssociationsPage.prototype.ionViewWillEnter = function () {
        this.occupation = this.navParams.get('occupation');
        this.searchInput = this.navParams.get('searchInput');
    };
    OccupationAssociationsPage.prototype.goBack = function () {
        var searchInput = this.searchInput;
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__list_list__["a" /* ListPage */], {
            searchInput: searchInput
        });
    };
    return OccupationAssociationsPage;
}());
OccupationAssociationsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-occupationAssociations',template:/*ion-inline-start:"E:\boostrs\ngAdmin\src\pages\occupationAssociations\occupationAssociations.html"*/'<ion-header>\n\n  <ion-navbar> \n\n    <ion-buttons left>\n\n      <button class="backButton" ion-button (click)="goBack()">\n\n        <ion-icon name="ios-arrow-back-outline"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    \n\n    <ion-title>Occupation Associations</ion-title>\n\n    \n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="gradient">\n\n  <hexagon-component [hidden]="!occupation" [occupation]="occupation"></hexagon-component>\n\n</ion-content>'/*ion-inline-end:"E:\boostrs\ngAdmin\src\pages\occupationAssociations\occupationAssociations.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__services_OccupationsService__["a" /* OccupationsService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_OccupationsService__["a" /* OccupationsService */]])
], OccupationAssociationsPage);

//# sourceMappingURL=occupationAssociations.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_merge__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_merge___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_merge__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_forkJoin__ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_forkJoin___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_forkJoin__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_OccupationsService__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_SkillsService__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_AssociationsService__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_d3__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_Observable__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__list_list__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











;
var HomePage = (function () {
    function HomePage(navCtrl, platform, occupationsService, associationsService, skillsService, storage, navParams, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.occupationsService = occupationsService;
        this.associationsService = associationsService;
        this.skillsService = skillsService;
        this.storage = storage;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.showResetGraphButton = { codeISCO: false, node: false };
        this.svgGraph = {};
        this.svgGraph.nodes = [];
        this.svgGraph.links = [];
    }
    HomePage.prototype.presentLoadingDefault = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
    };
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.presentLoadingDefault();
        this.showResetGraphButton = { codeISCO: false, node: false };
        this.searchInput = this.navParams.get('searchInput');
        this.occupationsService.getOccupationsForMinorCategory(this.searchInput).subscribe(function (result) { return _this.getOccupationsForMinorCategorySuccess(result); }, function (error) { return _this.getOccupationsForMinorCategoryError(error); }, function () { });
    };
    HomePage.prototype.goBack = function () {
        this.resetGraphHighlighting();
        var searchInput = this.searchInput;
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__list_list__["a" /* ListPage */], {
            searchInput: searchInput
        });
    };
    HomePage.prototype.tryRender = function () {
        this.svg = __WEBPACK_IMPORTED_MODULE_8_d3__["k" /* select */]("svg");
        var width = +this.svg.attr("width");
        var height = +this.svg.attr("height");
        this.color = __WEBPACK_IMPORTED_MODULE_8_d3__["i" /* scaleOrdinal */](__WEBPACK_IMPORTED_MODULE_8_d3__["j" /* schemeCategory20 */]);
        this.simulation = __WEBPACK_IMPORTED_MODULE_8_d3__["g" /* forceSimulation */]()
            .force("link", __WEBPACK_IMPORTED_MODULE_8_d3__["e" /* forceLink */]().id(function (d) { return d.id; }))
            .force("charge", __WEBPACK_IMPORTED_MODULE_8_d3__["f" /* forceManyBody */]().strength(-50).distanceMax(600).distanceMin(1))
            .force("center", __WEBPACK_IMPORTED_MODULE_8_d3__["d" /* forceCenter */](width / 2, height / 2));
        this.render(this.svgGraph);
    };
    HomePage.prototype.ticked = function () {
        this.link
            .attr("x1", function (d) { return d.source.x; })
            .attr("y1", function (d) { return d.source.y; })
            .attr("x2", function (d) { return d.target.x; })
            .attr("y2", function (d) { return d.target.y; });
        this.node
            .attr("cx", function (d) { return d.x; })
            .attr("cy", function (d) { return d.y; })
            .attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")"; });
    };
    HomePage.prototype.render = function (graph) {
        var _this = this;
        this.node = this.svg.append("g")
            .attr("class", "nodes")
            .selectAll(".node")
            .data(graph.nodes)
            .enter().append("g")
            .attr("class", function (d) { return "node gNodes gNodes-" + d.codeISCO + " gNodes-" + d.id; })
            .on("click", function (d) { console.log('highlight'); _this.highlightNode(d); })
            .call(__WEBPACK_IMPORTED_MODULE_8_d3__["b" /* drag */]()
            .on("start", function (d) { return _this.dragstarted(d); })
            .on("drag", function (d) { return _this.dragged(d); })
            .on("end", function (d) { return _this.dragended(d); }));
        this.node.append("circle")
            .attr("r", function (node) { return node.inMinorISCO ? 15 : 10; })
            .attr("class", function (d) { return "circles circles-" + d.codeISCO + " circles-" + d.id; })
            .attr("fill", function (d) {
            var groupColor = _this.color(d.group);
            d.groupColor = groupColor;
            _this.uniqueISCO[_this.uniqueISCO.findIndex(function (item) { return item.codeISCO === d.codeISCO; })].groupColor = groupColor;
            return groupColor;
        });
        this.node.append("text")
            .text(function (d) { return d.shortDescription; });
        this.link = this.svg.append("g")
            .attr("class", "links")
            .selectAll("line")
            .data(graph.links)
            .enter()
            .append("line")
            .attr("class", function (d) { return "link link-" + d.source + " link-" + d.target; })
            .attr("stroke-width", function (d) { return 1; });
        this.node.append("title")
            .text(function (d) { return "Name: " + d.shortDescription + ", ISCO: " + d.codeISCO; });
        this.simulation
            .nodes(graph.nodes)
            .on("tick", function () { return _this.ticked(); });
        this.simulation.force("link")
            .links(graph.links);
        //add more negative attraction between nodes
        //add histroy in which you explain color - isco + name of code isco
        this.svg.call(__WEBPACK_IMPORTED_MODULE_8_d3__["m" /* zoom */]().on("zoom", function () {
            _this.svg.selectAll(".links, .nodes").attr("transform", __WEBPACK_IMPORTED_MODULE_8_d3__["c" /* event */].transform);
        }));
    };
    HomePage.prototype.highlightNode = function (occupation) {
        var _this = this;
        if (!occupation.inMinorISCO)
            return;
        this.showResetGraphButton.codeISCO = false;
        this.svg.selectAll(".gNodes").attr("opacity", 0);
        this.svg.selectAll(".circles").attr("fill", "gray");
        this.svg.selectAll(".link").attr("opacity", 0);
        this.svg.select(".gNodes-" + occupation.id).attr("opacity", 1);
        this.svg.select(".circles-" + occupation.id).attr("fill", function (d) { return d.groupColor; });
        var occupationAssociationsCircles = [];
        this.svgGraph.links.forEach(function (d) {
            if (occupation.id == d.source.id) {
                occupationAssociationsCircles.push(d.target.id);
            }
            if (occupation.id == d.target.id) {
                occupationAssociationsCircles.push(d.source.id);
            }
        });
        occupationAssociationsCircles.forEach(function (occupationID) {
            _this.svg.select(".gNodes-" + occupationID).attr("opacity", 1);
            _this.svg.select(".circles-" + occupationID).attr("fill", function (d) { return d.groupColor; });
        });
        this.svg.selectAll(".link-" + occupation.id).attr("opacity", 1);
        this.showResetGraphButton.node = true;
    };
    HomePage.prototype.highlightISCO = function (codeISCO) {
        if (this.showResetGraphButton.node)
            return;
        this.svg.selectAll(".gNodes").attr("opacity", 0.3);
        this.svg.selectAll(".circles").attr("fill", "gray");
        this.svg.selectAll(".gNodes-" + codeISCO).attr("opacity", 1);
        this.svg.selectAll(".circles-" + codeISCO).attr("fill", function (d) { return d.groupColor; });
        this.showResetGraphButton.codeISCO = true;
    };
    HomePage.prototype.resetGraphHighlighting = function () {
        var _this = this;
        this.uniqueISCO.forEach(function (u) {
            _this.svg.selectAll(".gNodes-" + u.codeISCO).attr("opacity", 1);
            _this.svg.selectAll(".circles-" + u.codeISCO).attr("fill", u.groupColor);
        });
        if (this.showResetGraphButton.node) {
            this.svg.selectAll(".link").attr("opacity", 1);
            this.showResetGraphButton.node = false;
        }
        this.showResetGraphButton.codeISCO = false;
    };
    HomePage.prototype.dragged = function (d) {
        d.fx = __WEBPACK_IMPORTED_MODULE_8_d3__["c" /* event */].x;
        d.fy = __WEBPACK_IMPORTED_MODULE_8_d3__["c" /* event */].y;
    };
    HomePage.prototype.dragended = function (d) {
        if (!__WEBPACK_IMPORTED_MODULE_8_d3__["c" /* event */].active)
            this.simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    };
    HomePage.prototype.dragstarted = function (d) {
        if (!__WEBPACK_IMPORTED_MODULE_8_d3__["c" /* event */].active)
            this.simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    };
    HomePage.prototype.getOccupationsForMinorCategorySuccess = function (result) {
        var _this = this;
        this.occupations = result.occupations.map(function (occupation, index) { occupation.index = index; return occupation; });
        var occupationAssociationsObservable = [];
        this.occupations.forEach(function (occupation) {
            occupationAssociationsObservable.push(_this.occupationsService.getAssociatedOccupations(occupation.IRI));
        });
        __WEBPACK_IMPORTED_MODULE_9_rxjs_Observable__["Observable"].forkJoin(occupationAssociationsObservable)
            .subscribe(function (result) { return _this.getOccupationAndAssociations(result); }, function (err) { return console.log(err); }, function () { });
    };
    HomePage.prototype.getOccupationAndAssociations = function (responses) {
        var _this = this;
        //this.associatedOccupationCategorization = responses;
        var associatedOccupations = [].concat.apply([], responses.map(function (r) { return r.data; }));
        var uniqueOccupations = this.occupations.concat(associatedOccupations).filter(function (occupation, index, self) {
            return self.findIndex(function (occ) { return occ.IRI === occupation.IRI; }) === index;
        });
        this.uniqueISCO = this.occupations.concat(associatedOccupations).filter(function (occupation, index, self) {
            return self.findIndex(function (occ) { return occ.codeISCO === occupation.codeISCO; }) === index;
        }).map(function (occupation) { return { codeISCO: occupation.codeISCO }; }).sort(function (a, b) { return a.codeISCO > b.codeISCO ? 1 : -1; });
        this.occupationsService.getCodeISCODescription(this.uniqueISCO.map(function (u) { return u.codeISCO; })).subscribe(function (response) { return _this.getCodeISCODescriptonSuccess(response, uniqueOccupations, responses); }, function (error) { return _this.getCodeISCODescriptionError(error); }, function () { });
    };
    HomePage.prototype.getCodeISCODescriptonSuccess = function (response, uniqueOccupations, responses) {
        var _this = this;
        var responseArray = response.categories;
        responseArray.sort(function (a, b) { return a.codeISCO > b.codeISCO ? 1 : -1; }).forEach(function (category, index) {
            _this.uniqueISCO[index].description = category.description;
        });
        this.svgGraph.nodes = uniqueOccupations.map(function (uOcc, index) { uOcc.id = uOcc._id; uOcc.group = uOcc.codeISCO; uOcc.inMinorISCO = index < 50; return uOcc; });
        this.svgGraph.links = [];
        this.svgGraph.links = [].concat.apply([], responses.map(function (r, index) {
            var mainOccupation = _this.occupations[index];
            return r.data.map(function (occupation) {
                return { target: occupation._id, source: mainOccupation._id, "value": occupation.score };
            });
        }));
        this.loading.dismiss();
        this.tryRender();
    };
    HomePage.prototype.getCodeISCODescriptionError = function (error) {
    };
    HomePage.prototype.draw = function () {
        var config = {
            container_id: "viz",
            encrypted: "ENCRYPTION_ON",
            server_url: "bolt://hobby-ldadcoiekhacgbkedkhlopal.dbs.graphenedb.com:24786",
            server_user: "expressapi",
            server_password: "b.b7l60l7o8XlW.8bxIHbp4bZ6P1ViN",
            labels: {
                "Occupation": {
                    "name": "name",
                    "codeISCO": "codeISCO",
                    "id": "id"
                }
            },
            relationships: {
                "RELTYPE": {}
            },
            initial_cypher: "MATCH p=()-[r:RELTYPE]->() RETURN p LIMIT 25"
        };
        this.viz = new NeoVis.default(config);
        this.viz.render();
        console.log(this.viz);
    };
    HomePage.prototype.getOccupationsForMinorCategoryError = function (error) {
        console.log(error);
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"E:\boostrs\ngAdmin\src\pages\home\home.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons left>\n            <button class="backButton" ion-button (click)="goBack()">\n                <ion-icon name="ios-arrow-back-outline"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title>Association Graph</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding no-bounce>\n    <ion-grid>\n        <ion-row>\n            <ion-col col col-lg-3>\n                <ion-scroll style="width:100%;height:80vh" scrollY="true">\n                    <ion-row scroll="true">\n                        <ion-col *ngIf="showResetGraphButton && (showResetGraphButton.codeISCO || showResetGraphButton.node)" col col-12 (click)="resetGraphHighlighting()">\n                            <button ion-button color="light" full>Reset Graph</button>\n                        </ion-col>\n                        <ion-col col col-12 *ngFor="let item of uniqueISCO; let i = index;" (click)="highlightISCO(item.codeISCO)">\n                            <div style="display:inline-block">ISCO: {{item.codeISCO}}</div>\n                            <div style="display:inline-block; width:15px; height:15px; border-radius:5px; margin-top:5px;" [style.background-color]="item.groupColor"></div>\n                            <div>{{item.description}}</div>\n                        </ion-col>\n                    </ion-row>\n                </ion-scroll>\n            </ion-col>\n            <ion-col col col-lg-9 class="svgComponent">\n                <svg preserveAspectRatio="none" class="graph" [attr.height.px]="this.platform.height() * 80/100" [attr.width.px]="this.platform.width() < 960 ? this.platform.width() * 95 / 100 : this.platform.width() * 70/100">\n                </svg>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n    <!-- <div id="viz"></div> -->\n</ion-content>'/*ion-inline-end:"E:\boostrs\ngAdmin\src\pages\home\home.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_5__services_OccupationsService__["a" /* OccupationsService */], __WEBPACK_IMPORTED_MODULE_6__services_SkillsService__["a" /* SkillsService */], __WEBPACK_IMPORTED_MODULE_7__services_AssociationsService__["a" /* AssociationsService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_5__services_OccupationsService__["a" /* OccupationsService */],
        __WEBPACK_IMPORTED_MODULE_7__services_AssociationsService__["a" /* AssociationsService */],
        __WEBPACK_IMPORTED_MODULE_6__services_SkillsService__["a" /* SkillsService */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SkillsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__apiUrl__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SkillsService = (function () {
    function SkillsService(http, storage) {
        this.http = http;
        this.storage = storage;
        this.skillsUrl = __WEBPACK_IMPORTED_MODULE_4__apiUrl__["a" /* default */] + "/api/skills";
    }
    SkillsService.prototype.get = function () {
        return this.http.get(this.skillsUrl);
    };
    SkillsService.prototype.getFiveSkills = function () {
        return this.http.get(this.skillsUrl + "/fiveskills");
    };
    SkillsService.prototype.getCommonSkills = function (occupationIRI1, occupationIRI2) {
        if (occupationIRI1 > occupationIRI2) {
            var aux = occupationIRI1;
            occupationIRI1 = occupationIRI2;
            occupationIRI2 = aux;
        }
        return this.http.post(this.skillsUrl + "/occupationsCommonSkills", {
            occupationIRI1: occupationIRI1,
            occupationIRI2: occupationIRI2
        });
    };
    return SkillsService;
}());
SkillsService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], SkillsService);

//# sourceMappingURL=SkillsService.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_google_maps_google_maps__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_google_maps_cluster_google_maps_cluster__ = __webpack_require__(391);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MapsPage = (function () {
    function MapsPage(platform, maps, mapCluster) {
        this.platform = platform;
        this.maps = maps;
        this.mapCluster = mapCluster;
        this.mapWasInitialised = false;
    }
    MapsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.mapLoaded = _this.maps.init(_this.mapElement.nativeElement, _this.pleaseConnect.nativeElement).then(function (map) {
                _this.mapCluster.addCluster(map);
            });
        });
    };
    return MapsPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('map'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], MapsPage.prototype, "mapElement", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('pleaseConnect'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], MapsPage.prototype, "pleaseConnect", void 0);
MapsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-maps',template:/*ion-inline-start:"E:\boostrs\ngAdmin\src\pages\maps\maps.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Map Clusters</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <div #pleaseConnect id="please-connect">\n    <p>Please connect to the Internet...</p>\n  </div>\n\n  <div #map id="map" style="width:100%;height:100%;"></div>\n\n</ion-content>'/*ion-inline-end:"E:\boostrs\ngAdmin\src\pages\maps\maps.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__providers_google_maps_google_maps__["a" /* GoogleMapsProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_google_maps_cluster_google_maps_cluster__["a" /* GoogleMapsClusterProvider */]])
], MapsPage);

//# sourceMappingURL=maps.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleMapsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__connectivity_connectivity__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(390);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the GoogleMapsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var GoogleMapsProvider = (function () {
    function GoogleMapsProvider(connectivityService, geolocation) {
        this.connectivityService = connectivityService;
        this.geolocation = geolocation;
        this.mapInitialised = false;
        this.apiKey = "AIzaSyCL7fH8Y5xVLxWskIi36Z7i3nQjLmMEp_k";
    }
    GoogleMapsProvider.prototype.init = function (mapElement, pleaseConnect) {
        this.mapElement = mapElement;
        this.pleaseConnect = pleaseConnect;
        return this.loadGoogleMaps();
    };
    GoogleMapsProvider.prototype.loadGoogleMaps = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (typeof google == "undefined" || typeof google.maps == "undefined") {
                console.log("Google maps JavaScript needs to be loaded.");
                _this.disableMap();
                if (_this.connectivityService.isOnline()) {
                    window['mapInit'] = function () {
                        _this.initMap().then(function (map) {
                            resolve(map);
                        });
                        _this.enableMap();
                    };
                    var script = document.createElement("script");
                    script.id = "googleMaps";
                    if (_this.apiKey) {
                        script.src = 'http://maps.google.com/maps/api/js?key=' + _this.apiKey + '&callback=mapInit';
                    }
                    else {
                        script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
                    }
                    document.body.appendChild(script);
                }
            }
            else {
                if (_this.connectivityService.isOnline()) {
                    _this.initMap();
                    _this.enableMap();
                }
                else {
                    _this.disableMap();
                }
            }
            _this.addConnectivityListeners();
        });
    };
    GoogleMapsProvider.prototype.initMap = function () {
        var _this = this;
        this.mapInitialised = true;
        return new Promise(function (resolve) {
            _this.geolocation.getCurrentPosition().then(function (position) {
                var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                var mapOptions = {
                    minZoom: 3, maxZoom: 12,
                    center: latLng,
                    zoom: 2,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                _this.map = new google.maps.Map(_this.mapElement, mapOptions);
                resolve(_this.map);
            }).catch(function (err) {
                if (err.code === 1) {
                    var latLng = new google.maps.LatLng(47.751569, 1.675063);
                    var mapOptions = {
                        minZoom: 3, maxZoom: 15,
                        center: latLng,
                        zoom: 2,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    _this.map = new google.maps.Map(_this.mapElement, mapOptions);
                    resolve(_this.map);
                }
            });
        });
    };
    GoogleMapsProvider.prototype.disableMap = function () {
        if (this.pleaseConnect) {
            this.pleaseConnect.style.display = "block";
        }
    };
    GoogleMapsProvider.prototype.enableMap = function () {
        if (this.pleaseConnect) {
            this.pleaseConnect.style.display = "none";
        }
    };
    GoogleMapsProvider.prototype.addConnectivityListeners = function () {
        var _this = this;
        this.connectivityService.watchOnline().subscribe(function () {
            console.log("online");
            setTimeout(function () {
                if (typeof google == "undefined" || typeof google.maps == "undefined") {
                    _this.loadGoogleMaps();
                }
                else {
                    if (!_this.mapInitialised) {
                        _this.initMap();
                    }
                    _this.enableMap();
                }
            }, 2000);
        });
        this.connectivityService.watchOffline().subscribe(function () {
            console.log("offline");
            _this.disableMap();
        });
    };
    return GoogleMapsProvider;
}());
GoogleMapsProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__connectivity_connectivity__["a" /* ConnectivityProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */]])
], GoogleMapsProvider);

//# sourceMappingURL=google-maps.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectivityProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ConnectivityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ConnectivityProvider = (function () {
    function ConnectivityProvider(platform, network) {
        var _this = this;
        this.platform = platform;
        this.network = network;
        this.onDevice = this.platform.is('cordova');
        var disconnectSubscription = this.network.onDisconnect().subscribe(function () {
            console.log('offline');
            _this.connection = false;
        });
        var connectSubscription = this.network.onConnect().subscribe(function () {
            setTimeout(function () {
                console.log('online');
                _this.connection = true;
            }, 3000);
        });
    }
    ConnectivityProvider.prototype.isOnline = function () {
        if (this.onDevice && this.connection) {
            return true;
        }
        else {
            return navigator.onLine;
        }
    };
    ConnectivityProvider.prototype.isOffline = function () {
        if (this.onDevice && this.connection) {
            return true;
        }
        else {
            return !navigator.onLine;
        }
    };
    ConnectivityProvider.prototype.watchOnline = function () {
        return this.network.onConnect();
    };
    ConnectivityProvider.prototype.watchOffline = function () {
        return this.network.onDisconnect();
    };
    return ConnectivityProvider;
}());
ConnectivityProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
], ConnectivityProvider);

//# sourceMappingURL=connectivity.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleMapsClusterProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_node_js_marker_clusterer__ = __webpack_require__(753);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_node_js_marker_clusterer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_node_js_marker_clusterer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the GoogleMapsClusterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var GoogleMapsClusterProvider = (function () {
    function GoogleMapsClusterProvider(http) {
        this.http = http;
        console.log('Hello GoogleMapsCluster Provider');
        this.locations = [
            { lat: -31.563910, lng: 147.154312, mainOccupation: "Advertising Manager", codeISCO: "ISCO 3" },
            { lat: 52.486243, lng: -1.890401, mainOccupation: "Public finance accountant", country: "England", city: "Birmingham", codeISCO: 2411 },
            { lat: 51.507351, lng: -0.127758, mainOccupation: "Tax advisor", country: "England", city: "London", codeISCO: 2411 },
            { lat: 53.480759, lng: -2.242631, mainOccupation: "Design engineer", country: "England", city: "Manchester", codeISCO: 2149 },
            { lat: 51.517371, lng: -0.137778, mainOccupation: "Audit supervisor", country: "England", city: "London", codeISCO: 2411 },
            { lat: 51.547381, lng: -0.117718, mainOccupation: "HR manager", country: "England", city: "London", codeISCO: 1212 },
            { lat: 51.527391, lng: -0.127728, mainOccupation: "Data scientist", country: "England", city: "London", codeISCO: 2511 },
            { lat: 43.610769, lng: 3.876716, mainOccupation: "Tehnical director", country: "France", city: "Montpeiller", codeISCO: 2654 },
            { lat: 48.826614, lng: 2.372222, mainOccupation: "Specialised Doctor", country: "France", city: "Paris", codeISCO: 2212 },
            { lat: 45.764043, lng: 4.835659, mainOccupation: "Financial broker", country: "France", city: "Lyon", codeISCO: 3311 },
            { lat: 47.218371, lng: -1.553621, mainOccupation: "Company directors", country: "France", city: "Nantes", codeISCO: 2654 },
            { lat: 48.856614, lng: 2.352222, mainOccupation: "Civil service administrative officers", country: "France", city: "Paris", codeISCO: 2422 },
            { lat: 43.552847, lng: 7.017369, mainOccupation: "Airplane transport pilot", country: "France", city: "Cannes", codeISCO: 3153 },
            { lat: 52.560007, lng: 13.424954, mainOccupation: "Technical director", country: "Germany", city: "Berlin", codeISCO: 2654 },
            { lat: 52.550007, lng: 13.444954, mainOccupation: "Government minister", country: "Germany", city: "Berlin", codeISCO: 1111 },
            { lat: 48.135125, lng: 11.581980, mainOccupation: "Corporate Investment banker", country: "Germany", city: "Munchen", codeISCO: 2412 },
            { lat: 52.510007, lng: 13.424954, mainOccupation: "Sales manager", country: "Germany", city: "Berlin", codeISCO: 1221 },
            { lat: 50.110922, lng: 8.682127, mainOccupation: "Medical laboratory manager", country: "Germany", city: "Frankfurt", codeISCO: 1349 },
            { lat: 53.551085, lng: 9.993682, mainOccupation: "Lawyer", country: "Germany", city: "Hamburg", codeISCO: 2611 },
            { lat: 39.469907, lng: -0.376288, mainOccupation: "Business Economics Researcher", country: "Spain", city: "Valencia", codeISCO: 2631 },
            { lat: 41.395064, lng: 2.123403, mainOccupation: "Operations manager", country: "Spain", city: "Barcelona", codeISCO: 1321 },
            { lat: 41.335064, lng: 2.153403, mainOccupation: "Chief Fire Officer", country: "Spain", city: "Barcelona", codeISCO: 1349 },
            { lat: 40.416775, lng: -3.703790, mainOccupation: "Talent Agent", country: "Spain", city: "Madrid", codeISCO: 3339 },
            { lat: 40.416775, lng: -3.703790, mainOccupation: "Marketing Manager", country: "Spain", city: "Madrid", codeISCO: 1221 },
            { lat: 41.325064, lng: 2.103403, mainOccupation: "Telecommunications Manager", country: "Spain", city: "Barcelona", codeISCO: 1330 },
        ];
    }
    GoogleMapsClusterProvider.prototype.addCluster = function (map) {
        var _this = this;
        console.log(map, google.maps);
        if (google.maps) {
            console.log('Init clusters');
            var infowindow = new google.maps.InfoWindow();
            //Convert locations into array of markers
            var markers = this.locations.map(function (location) {
                return new google.maps.Marker({
                    position: location
                });
            });
            markers.forEach(function (marker, index) {
                var self = _this;
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.setContent('<div><strong>Main Occupation</strong><br>' +
                        'Occupation Name: ' + self.locations[index].mainOccupation + '<br>' +
                        'Code ISCO: ' + self.locations[index].codeISCO + '</div>');
                    infowindow.open(map, this);
                });
            });
            this.markerCluster = new __WEBPACK_IMPORTED_MODULE_2_node_js_marker_clusterer__(map, markers, { imagePath: 'assets/images/m' });
        }
        else {
            console.warn('Google maps needs to be loaded before adding a cluster');
        }
    };
    return GoogleMapsClusterProvider;
}());
GoogleMapsClusterProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], GoogleMapsClusterProvider);

//# sourceMappingURL=google-maps-cluster.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_OccupationsService__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardPage = (function () {
    function DashboardPage(occupationsService) {
        var _this = this;
        this.occupationsService = occupationsService;
        // options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.xAxisLabel = 'Number of common skills';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Number of associations';
        this.view = [1200, 200];
        this.colorScheme = {
            domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
        };
        this.single = [
            {
                "name": "Users",
                "value": 2
            },
            {
                "name": "Occupations",
                "value": 2950
            },
            {
                "name": "Associations",
                "value": 114921
            },
            {
                "name": "Occupation Associations",
                "value": 1018434
            },
            {
                "name": "Skills",
                "value": 13492
            },
            {
                "name": "Categories",
                "value": 500
            }
        ];
        this.singleHistogram = [];
        this.singlePieChart = [];
        this.occupationsService.getOccupationAssociationsHistogram()
            .subscribe(function (response) { return _this.occupationAssociationsHistogramSuccess(response); }, function (error) { return _this.occupationAssociationsHistogramError(error); }, function () { });
    }
    DashboardPage.prototype.occupationAssociationsHistogramSuccess = function (response) {
        this.singleHistogram = response.histogramArray.map(function (element) {
            return { name: element._id, value: Math.log(element.count) };
        });
        this.singlePieChart = [
            { "name": '1 Skill', "value": response.histogramArray[0].count },
            { "name": '2 Skills', "value": response.histogramArray[1].count },
            { "name": '3 Skills', "value": response.histogramArray[2].count },
            { "name": '4-6 Skills', "value": response.histogramArray.slice(3, 5).reduce(function (sum, h) { return sum + h.count; }, 0) },
            { "name": '7-15 Skills', "value": response.histogramArray.slice(6, 14).reduce(function (sum, h) { return sum + h.count; }, 0) },
            { "name": '16-50 Skills', "value": response.histogramArray.slice(15, 49).reduce(function (sum, h) { return sum + h.count; }, 0) },
            { "name": '50-118 Skills', "value": response.histogramArray.slice(50, response.histogramArray.length - 1).reduce(function (sum, h) { return sum + h.count; }, 0) },
        ];
    };
    DashboardPage.prototype.occupationAssociationsHistogramError = function (error) {
    };
    DashboardPage.prototype.clickPie = function (event) {
    };
    DashboardPage.prototype.onSelect = function (event) {
    };
    return DashboardPage;
}());
DashboardPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-dashboard',template:/*ion-inline-start:"E:\boostrs\ngAdmin\src\pages\dashboard\dashboard.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Dashboard Page</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ngx-charts-number-card [scheme]="colorScheme" [results]="single" (select)="onSelect($event)">\n\n        </ngx-charts-number-card>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ngx-charts-bar-vertical [view]="view" [scheme]="colorScheme" [results]="singleHistogram" [gradient]="gradient" [xAxis]="showXAxis"\n\n          [yAxis]="showYAxis" [showXAxisLabel]="showXAxisLabel" [showYAxisLabel]="showYAxisLabel" [xAxisLabel]="xAxisLabel"\n\n          [yAxisLabel]="yAxisLabel" (select)="onSelect($event)">\n\n        </ngx-charts-bar-vertical>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ngx-charts-pie-grid [view]="view" [scheme]="colorScheme" [results]="singlePieChart" (select)="clickPie($event)">\n\n        </ngx-charts-pie-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"E:\boostrs\ngAdmin\src\pages\dashboard\dashboard.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_1__services_OccupationsService__["a" /* OccupationsService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_OccupationsService__["a" /* OccupationsService */]])
], DashboardPage);

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(410);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_AuthInterceptor__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_list_list__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_occupationAssociations_occupationAssociations__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_maps_maps__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_hexagon_hexagon__ = __webpack_require__(754);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_status_bar__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_splash_screen__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__swimlane_ngx_charts__ = __webpack_require__(756);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_platform_browser_animations__ = __webpack_require__(764);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_ng2_cytoscape_dist__ = __webpack_require__(766);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_ng2_cytoscape_dist___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_19_ng2_cytoscape_dist__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_google_maps_google_maps__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_connectivity_connectivity__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_google_maps_cluster_google_maps_cluster__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_network__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_geolocation__ = __webpack_require__(390);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__["a" /* DashboardPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_occupationAssociations_occupationAssociations__["a" /* OccupationAssociationsPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_maps_maps__["a" /* MapsPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_hexagon_hexagon__["a" /* HexagonComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
            __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_17__swimlane_ngx_charts__["NgxChartsModule"],
            __WEBPACK_IMPORTED_MODULE_18__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_19_ng2_cytoscape_dist__["NgCytoscapeModule"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {}, {
                links: []
            }),
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__["a" /* DashboardPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_occupationAssociations_occupationAssociations__["a" /* OccupationAssociationsPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_maps_maps__["a" /* MapsPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_hexagon_hexagon__["a" /* HexagonComponent */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_15__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_16__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_23__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_24__ionic_native_geolocation__["a" /* Geolocation */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            {
                provide: __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HTTP_INTERCEPTORS */],
                useClass: __WEBPACK_IMPORTED_MODULE_5__services_AuthInterceptor__["a" /* AuthInterceptor */],
                multi: true,
            },
            __WEBPACK_IMPORTED_MODULE_20__providers_google_maps_google_maps__["a" /* GoogleMapsProvider */],
            __WEBPACK_IMPORTED_MODULE_21__providers_connectivity_connectivity__["a" /* ConnectivityProvider */],
            __WEBPACK_IMPORTED_MODULE_22__providers_google_maps_cluster_google_maps_cluster__["a" /* GoogleMapsClusterProvider */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthInterceptor = (function () {
    function AuthInterceptor(storage) {
        this.storage = storage;
    }
    AuthInterceptor.prototype.intercept = function (req, next) {
        if (!req.body || (!req.body.hasOwnProperty('withToken') || req.body.withToken)) {
            var authReq = req.clone({ headers: req.headers.set('x-access-token', localStorage.getItem("token")) });
            return next.handle(authReq);
        }
        else {
            return next.handle(req);
        }
    };
    return AuthInterceptor;
}());
AuthInterceptor = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
], AuthInterceptor);

//# sourceMappingURL=AuthInterceptor.js.map

/***/ }),

/***/ 457:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_maps_maps__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__ = __webpack_require__(392);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Dashboard', component: __WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__["a" /* DashboardPage */] },
            { title: 'List', component: __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */] },
            { title: 'Maps', component: __WEBPACK_IMPORTED_MODULE_6__pages_maps_maps__["a" /* MapsPage */] },
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // console.log('Width: ' + this.platform.width());
            // console.log('Height: ' + this.platform.height());
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.logout = function () {
        localStorage.removeItem('token');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */]);
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"E:\boostrs\ngAdmin\src\app\app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content class="menuPage">\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n      <button menuClose ion-item (click)="logout()">\n        Logout\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"E:\boostrs\ngAdmin\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export LoginModel */
/* unused harmony export LoginResponseModel */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__apiUrl__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginModel = (function () {
    function LoginModel() {
    }
    return LoginModel;
}());

var LoginResponseModel = (function () {
    function LoginResponseModel() {
    }
    return LoginResponseModel;
}());

var AuthenticationService = (function () {
    function AuthenticationService(http) {
        this.http = http;
        this.authenticationUrl = __WEBPACK_IMPORTED_MODULE_3__apiUrl__["a" /* default */] + "/authenticate";
    }
    AuthenticationService.prototype.post = function (loginForm) {
        return this.http.post(this.authenticationUrl, { name: loginForm.username, password: loginForm.password, withToken: false });
    };
    return AuthenticationService;
}());
AuthenticationService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
], AuthenticationService);

//# sourceMappingURL=AuthenticationService.js.map

/***/ }),

/***/ 468:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssociationsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__apiUrl__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AssociationsService = (function () {
    function AssociationsService(http, httpClient) {
        this.http = http;
        this.httpClient = httpClient;
        this.associationUrl = __WEBPACK_IMPORTED_MODULE_4__apiUrl__["a" /* default */] + "/api/associations";
    }
    AssociationsService.prototype.get = function () {
        return this.http.get(this.associationUrl, { headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'x-access-token': localStorage.getItem('token') }) })
            .toPromise()
            .then(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    AssociationsService.prototype.postOcuppationSkills = function (occupationIRI) {
        return this.http.post(this.associationUrl + '/ocupationSkills', { occupationIRI: occupationIRI }, { headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'x-access-token': localStorage.getItem('token') }) })
            .toPromise()
            .then(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    AssociationsService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    return AssociationsService;
}());
AssociationsService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClient */]])
], AssociationsService);

//# sourceMappingURL=AssociationsService.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_OccupationsService__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__occupationAssociations_occupationAssociations__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(298);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ListPage = (function () {
    function ListPage(navCtrl, navParams, occupationsService, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.occupationsService = occupationsService;
        this.alertCtrl = alertCtrl;
        this.occupationValidator = new RegExp("^[a-zA-Z ]*$");
        this.categoryTypes = [
            'major',
            'subMajor',
            'minor',
            'unit',
            'occupations'
        ];
        this.searchInput = '';
        this.categories = [];
        this.occupations = [];
    }
    ListPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.searchPlaceholder = 'Search by codeISCO or by Occupation Name';
        if (this.navParams.get('searchInput')) {
            this.searchInput = this.navParams.get('searchInput');
            setTimeout(function () { _this.searchItems(); }, 1000);
        }
        else {
            this.searchInput = '';
        }
        this.getOccupationCategories();
    };
    ListPage.prototype.getOccupationCategories = function (categoryType, codeISCOLike) {
        var _this = this;
        if (categoryType === void 0) { categoryType = 0; }
        if (codeISCOLike === void 0) { codeISCOLike = ''; }
        this.occupationsService
            .getOccupationCategories(this.categoryTypes[categoryType], codeISCOLike)
            .subscribe(function (response) { return _this.occupationCategoriesSuccess(response); }, function (error) { return _this.occupationsCategoriesError(error); }, function () { });
    };
    ListPage.prototype.getOccupationsByCodeISCO = function (codeISCO) {
        var _this = this;
        this.occupationsService
            .getOccupationByCodeISCO(codeISCO)
            .subscribe(function (response) { return _this.getOccupationsSuccess(response); }, function (error) { return _this.getOccupationsError(error); }, function () { });
    };
    ListPage.prototype.getOccupationsByName = function (occupationName) {
        var _this = this;
        this.occupationsService
            .getOccupationByName(occupationName)
            .subscribe(function (response) { return _this.getOccupationsSuccess(response); }, function (error) { return _this.getOccupationsError(error); }, function () { });
    };
    ListPage.prototype.presentAlert = function (alertContent) {
        var alert = this.alertCtrl.create(alertContent);
        alert.present();
        //this.searchInput = '';
        //this.searchItems();
    };
    ListPage.prototype.occupationsCategoriesError = function (error) {
    };
    ListPage.prototype.occupationCategoriesSuccess = function (response) {
        this.occupations = [];
        this.categories = response.categories;
    };
    ListPage.prototype.getOccupationsSuccess = function (response) {
        this.categories = [];
        this.occupations = response.occupations;
    };
    ListPage.prototype.getOccupationsError = function (error) {
    };
    ListPage.prototype.searchItems = function (event) {
        if (event === void 0) { event = null; }
        if (!isNaN(this.searchInput)) {
            this.inputIsNaN = false;
            if (this.searchInput.length < 4) {
                this.getOccupationCategories(this.searchInput.length, this.searchInput);
            }
            else {
                this.getOccupationsByCodeISCO(this.searchInput);
            }
        }
        else if (this.occupationValidator.test(this.searchInput)) {
            this.getOccupationsByName(this.searchInput);
            this.inputIsNaN = true;
        }
        else {
            this.presentAlert({ title: 'Invalid search', subTitle: 'Please use only numbers and spaces for Occupations search or only Numbers for CodeISCO', buttons: ['Dismiss'] });
            this.searchInput = '';
        }
    };
    ListPage.prototype.categoryExtendByClick = function (categoryCodeISCO) {
        this.searchInput = categoryCodeISCO;
        this.searchItems();
    };
    ListPage.prototype.occupationExtendByClick = function (occupation) {
        var searchInput = this.searchInput;
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__occupationAssociations_occupationAssociations__["a" /* OccupationAssociationsPage */], {
            occupation: occupation,
            searchInput: searchInput
        });
    };
    ListPage.prototype.goToGraphView = function (event) {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], {
            searchInput: this.searchInput
        });
        event.stopPropagation();
    };
    return ListPage;
}());
ListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-list',template:/*ion-inline-start:"E:\boostrs\ngAdmin\src\pages\list\list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      {{ inputIsNaN ? "Occupations" : categoryTypes[searchInput.length] }} \n      {{ !inputIsNaN ? searchInput.length < 4 ? "Categories" : \'\' : \'\'}}\n    </ion-title>\n\n    <ion-buttons end>\n      <button ion-button menuToggle>\n        <ion-icon name="contact" end></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-searchbar \n    [placeholder]="searchPlaceholder" \n    [debounce]="400" \n    [(ngModel)]="searchInput" \n    (ionInput)="searchItems($event)" \n    [showCancelButton]="shouldShowCancel">\n  </ion-searchbar>\n</ion-header>\n\n<ion-content class="gradient">\n  <ion-grid>\n    <ion-row class="card-background-page" *ngIf = "categories.length > 0 && this.searchInput.length < 4">\n      <ion-col col-12 col-md-6 col-lg-4 col-xl-3 *ngFor="let category of categories; let i = index;">\n        <ion-card (click)="categoryExtendByClick(category.codeISCO)">\n          <div class="card-title">\n            {{category.description}}\n          </div>\n          <div class="card-subtitle">\n            ISCO Code: {{category.codeISCO}}\n          </div>\n          <div *ngIf="searchInput.length >= 2" class="card-show-graph" (click)="goToGraphView($event)">\n              <ion-icon ios="logo-buffer" md="logo-buffer"></ion-icon>\n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row class="card-background-page" *ngIf="occupations.length > 0">\n      <ion-col col-12 col-md-6 col-lg-3 col-xl-2 *ngFor="let occupation of occupations; let i = index;">\n        <ion-card (click)="occupationExtendByClick(occupation)">\n          <div class="card-title">\n              {{occupation.shortDescription}}\n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"E:\boostrs\ngAdmin\src\pages\list\list.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__services_OccupationsService__["a" /* OccupationsService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_OccupationsService__["a" /* OccupationsService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], ListPage);

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OccupationsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__apiUrl__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




;
;
;
var OccupationsService = (function () {
    function OccupationsService(http) {
        this.http = http;
        this.occupationUrl = __WEBPACK_IMPORTED_MODULE_3__apiUrl__["a" /* default */] + "/api/occupations";
    }
    OccupationsService.prototype.get = function () {
        return this.http.get(this.occupationUrl);
    };
    OccupationsService.prototype.getOccupationAssociationsHistogram = function () {
        return this.http.get(this.occupationUrl + "/associationsHistogram");
    };
    OccupationsService.prototype.getOccupationsForMinorCategory = function (minorCodeISCO) {
        return this.http.post(this.occupationUrl + "/minorCodeISCO", {
            minorCodeISCO: minorCodeISCO
        });
    };
    OccupationsService.prototype.getOccupationByCodeISCO = function (codeISCO) {
        return this.http.post(this.occupationUrl + "/codeISCO", {
            codeISCO: codeISCO
        });
    };
    OccupationsService.prototype.getCodeISCODescription = function (codeISCOArray) {
        return this.http.post(this.occupationUrl + "/findCodeISCO", {
            codeISCOArray: codeISCOArray
        });
    };
    OccupationsService.prototype.getOccupationByName = function (occupationName) {
        return this.http.post(this.occupationUrl + "/name", {
            occupationName: occupationName
        });
    };
    OccupationsService.prototype.getAssociatedOccupations = function (occupationIRI) {
        return this.http.get(this.occupationUrl + ("/" + occupationIRI + "/associatedOccupations/"));
    };
    OccupationsService.prototype.getOccupationDetails = function (occupationIRI) {
        return this.http.get(this.occupationUrl + ("/iri/" + occupationIRI + "/"));
    };
    OccupationsService.prototype.getOccupationCategories = function (occupationCategory, codeISCO) {
        return this.http.post(this.occupationUrl + "/categories/", {
            occupationCategory: occupationCategory,
            codeISCO: codeISCO
        });
    };
    return OccupationsService;
}());
OccupationsService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
], OccupationsService);

//# sourceMappingURL=OccupationsService.js.map

/***/ }),

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HexagonComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__interfaces_Occupation__ = __webpack_require__(755);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_OccupationsService__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_d3__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_SkillsService__ = __webpack_require__(300);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HexagonComponent = (function () {
    function HexagonComponent(occupationsService, skillsService, platform) {
        this.occupationsService = occupationsService;
        this.skillsService = skillsService;
        this.platform = platform;
        this.selectedNode = null;
        this.selectedOccupation = null;
        this.associatedOccupations = [];
        this.graphJson = {};
        this.showSkillButtons = null;
        this.showSkillButtons = { common: false, diff: false };
    }
    HexagonComponent.prototype.showCommonSkills = function () {
        this.showSkillButtons.common = true;
        //this.skillsService.getCommonSkills(this.selectedNode.IRI, this.occupation.IRI).subscribe(skills => console.log(skills), err => console.log(err), () => {});
    };
    HexagonComponent.prototype.showDiffSkills = function () {
        this.showSkillButtons.diff = true;
    };
    HexagonComponent.prototype.goBack = function () {
        this.showSkillButtons.common = false;
        this.showSkillButtons.diff = false;
    };
    HexagonComponent.prototype.ngOnInit = function () {
        this.selectedOccupation = this.occupation;
        this.showSkillButtons = { common: false, diff: false };
    };
    HexagonComponent.prototype.ngOnChanges = function () {
        var _this = this;
        this.selectedOccupation = this.occupation;
        this.associatedOccupations = [];
        this.graphJson = {};
        __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("svg").selectAll("*").remove();
        this.occupation ?
            this.occupationsService
                .getAssociatedOccupations(this.occupation.IRI)
                .subscribe(function (response) { return _this.getAssociatedOccupationsSuccess(response); }, function (error) { return _this.getAssociatedOccupationsError(error); }, function () { }) : null;
    };
    HexagonComponent.prototype.getSelectedNodeDetails = function (selectedNode) {
        var _this = this;
        this.occupationsService
            .getOccupationDetails(selectedNode.IRI)
            .subscribe(function (response) { return _this.getSelectedNodeDetailsSuccess(response); }, function (error) { return _this.getSelectedNodeDetailsError(error); }, function () { });
    };
    HexagonComponent.prototype.getSelectedNodeDetailsSuccess = function (response) {
        this.selectedOccupation = response.occupation;
    };
    HexagonComponent.prototype.getSelectedNodeDetailsError = function (error) {
        console.log('Error: ', error);
    };
    HexagonComponent.prototype.initGraph = function () {
        var _this = this;
        this.svg = __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("svg");
        this.height = this.platform.width() < 400 ? this.platform.height() : this.platform.height() - 380;
        this.width = this.platform.width() < 960 ? this.platform.width() * 95 / 100 : this.platform.width() * 70 / 100;
        this.margin = { top: 20, right: 90, bottom: 30, left: 90 };
        this.hexagonTransitionFinished = true;
        this.svg.call(__WEBPACK_IMPORTED_MODULE_4_d3__["m" /* zoom */]().on("zoom", function () {
            _this.g.attr("transform", __WEBPACK_IMPORTED_MODULE_4_d3__["c" /* event */].transform);
        }));
        this.createOccupationAssociationsGraph();
    };
    HexagonComponent.prototype.generateHexagonMesh = function (nodes, centerPoint, diff, associatedHexagonRadius) {
        var hexCoords = [];
        var apothemPercentage = (Math.sqrt(3) / 2); //apothem of a hexagone is this number * radius 
        var radiusWithDiff = centerPoint.radius + diff;
        var apothemWithDiff = radiusWithDiff * apothemPercentage;
        var hexagonPositionsInMesh = [
            { x0: 2, y0: 0 },
            { x0: 1, y0: 1.5 },
            { x0: -1, y0: 1.5 },
            { x0: -2, y0: 0 },
            { x0: -1, y0: -1.5 },
            { x0: 1, y0: -1.5 },
        ];
        return nodes.map(function (node, index) {
            return {
                x: centerPoint.x + hexagonPositionsInMesh[index].x0 * apothemWithDiff,
                y: centerPoint.y + hexagonPositionsInMesh[index].y0 * radiusWithDiff,
                radius: associatedHexagonRadius
            };
        });
    };
    ;
    HexagonComponent.prototype.generateBadgesMesh = function (associatedHexagonCenterCoords) {
        var apothemPercentage = (Math.sqrt(3) / 2); //apothem of a hexagone is this number * radius 
        var trapezePositionInMesh = [
            {
                pointLeft: {
                    x: 6 / 8,
                    y: 5 / 8
                },
                pointRight: {
                    x: 6 / 8,
                    y: -5 / 8
                },
                badge: {
                    x: 7 / 8,
                    y: 0
                }
            },
            {
                pointLeft: {
                    x: -1 / 4,
                    y: 7 / 8
                },
                pointRight: {
                    x: 1,
                    y: 1 / 4,
                },
                badge: {
                    x: 1 / 2,
                    y: 5 / 8
                }
            },
            {
                pointLeft: {
                    x: -1,
                    y: 1 / 4
                },
                pointRight: {
                    x: 1 / 4,
                    y: 7 / 8
                },
                badge: {
                    x: -1 / 2,
                    y: 5 / 8
                }
            },
            {
                pointLeft: {
                    x: -6 / 8,
                    y: -5 / 8
                },
                pointRight: {
                    x: -6 / 8,
                    y: 5 / 8
                },
                badge: {
                    x: -7 / 8,
                    y: 0
                }
            },
            {
                pointLeft: {
                    x: 1 / 4,
                    y: -7 / 8
                },
                pointRight: {
                    x: -1,
                    y: -1 / 4
                },
                badge: {
                    x: -1 / 2,
                    y: -5 / 8
                }
            },
            {
                pointLeft: {
                    x: 1,
                    y: -1 / 4
                },
                pointRight: {
                    x: -1 / 4,
                    y: -7 / 8
                },
                badge: {
                    x: 1 / 2,
                    y: -5 / 8
                }
            }
        ];
        return associatedHexagonCenterCoords.map(function (hexCoords, index) {
            var apothem = apothemPercentage * hexCoords.radius;
            var hexagonSides = [
                { "x": hexCoords.radius * apothemPercentage + hexCoords.x, "y": -hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.radius * apothemPercentage + hexCoords.x, "y": hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.x, "y": hexCoords.y + hexCoords.radius },
                { "x": -hexCoords.radius * apothemPercentage + hexCoords.x, "y": hexCoords.radius / 2 + hexCoords.y },
                { "x": -hexCoords.radius * apothemPercentage + hexCoords.x, "y": -hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.x, "y": hexCoords.y - hexCoords.radius },
            ];
            return trapezePositionInMesh.map(function (sideCoords, index) {
                var hexagoneBadge = {
                    trapeze: [
                        {
                            x: hexCoords.x + sideCoords.pointLeft.x * apothem,
                            y: hexCoords.y + sideCoords.pointLeft.y * hexCoords.radius
                        },
                        {
                            x: hexCoords.x + sideCoords.pointRight.x * apothem,
                            y: hexCoords.y + sideCoords.pointRight.y * hexCoords.radius
                        }
                    ],
                    badge: {
                        x: hexCoords.x + sideCoords.badge.x * apothem,
                        y: hexCoords.y + sideCoords.badge.y * hexCoords.radius
                    }
                };
                hexagoneBadge.trapeze.push(hexagonSides[index], hexagonSides[(index + 1) % 6]);
                return hexagoneBadge;
            });
        });
    };
    HexagonComponent.prototype.rotateHexagons = function (numberOfRotations, callback) {
        var _this = this;
        if (this.hexagonTransitionFinished) {
            this.hexagonTransitionFinished = false;
            __WEBPACK_IMPORTED_MODULE_4_d3__["l" /* selectAll */]("." + this.classCommonToAllAssociatedOccupation)
                .transition()
                .duration(2000)
                .attr("transform", function (d) {
                var node = __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("." + d.customClass.split(' ').join('.'));
                var currentNodeIndex = parseInt(node.attr("index"));
                var newIndex = (currentNodeIndex + numberOfRotations) % 6;
                node.selectAll(".badge-" + currentNodeIndex).transition().delay(1000).style("fill", "none");
                node.selectAll(".badge.badge-" + newIndex + ", .trapeze.badge-" + newIndex).transition().delay(1000).style("fill", _this.badgeColor.trapeze);
                node.selectAll(".text.badge-" + newIndex).transition().delay(1000).style("fill", _this.badgeColor.text);
                node.attr("index", function () { return newIndex; });
                var x = _this.associatedOccupationsHexagonCoords[newIndex].x - parseInt(node.attr('coordsX'));
                var y = _this.associatedOccupationsHexagonCoords[newIndex].y - parseInt(node.attr('coordsY'));
                node.attr('translateX', x);
                node.attr('translateY', y);
                return "translate(" + x + ", " + y + ")";
            }).on("end", function (node, index) {
                if (index == 5)
                    callback();
            });
        }
    };
    HexagonComponent.prototype.associationNodeClick = function (d) {
        var _this = this;
        //If only coords of the main occupation are needed then d3MainOccupation can be deleted and the coords can be stored into a variable outside the d3 scope
        var d3MainOccupation = __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]('.mainOccupation');
        var clickedNode = __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("." + d.customClass.split(' ').join('.'));
        var clickedNodeTranslate = {
            x: clickedNode.attr('translateX'),
            y: clickedNode.attr('translateY')
        };
        //If hexagon is the on the right side of the main occupation, on the same horizontal
        if (parseInt(d3MainOccupation.attr('coordsY')) == (parseInt(clickedNode.attr('coordsY')) + parseInt(clickedNode.attr('translateY'))) &&
            parseInt(d3MainOccupation.attr('coordsX')) < (parseInt(clickedNode.attr('coordsX')) + parseInt(clickedNode.attr('translateX'))) &&
            this.hexagonTransitionFinished) {
            //Move hexagon and show relationship with skills
            clickedNode.attr("transform", function (d) {
                d.selected = !d.selected;
                if (d.selected) {
                    _this.selectedNode = d;
                    clickedNodeTranslate.x = parseInt(clickedNodeTranslate.x) + 200;
                    clickedNode.attr('translateX', clickedNodeTranslate.x);
                    _this.showSkills(true);
                    _this.getSelectedNodeDetails(_this.selectedNode);
                    var d3SelectedNode = __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("." + _this.selectedNode.customClass.split(' ').join('.'));
                    _this.skillNodeHexagonBadge(d3SelectedNode, false);
                    _this.generateSkillNodeText(true);
                }
                else {
                    _this.selectedOccupation = _this.occupation;
                    var d3SelectedNode = __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("." + _this.selectedNode.customClass.split(' ').join('.'));
                    _this.skillNodeHexagonBadge(d3SelectedNode, true);
                    _this.generateSkillNodeText(false);
                    _this.selectedNode = null;
                    clickedNodeTranslate.x = parseInt(clickedNodeTranslate.x) - 200;
                    clickedNode.attr('translateX', clickedNodeTranslate.x);
                    _this.showSkills(false);
                }
                return "translate(" + clickedNodeTranslate.x + "," + clickedNodeTranslate.y + ")";
            });
        }
        else {
            if (this.selectedNode) {
                var d3SelectedNode = __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("." + this.selectedNode.customClass.split(' ').join('.'));
                var d3SelectedNodeTranslate_1 = {
                    x: d3SelectedNode.attr('translateX'),
                    y: d3SelectedNode.attr('translateY')
                };
                d3SelectedNode.attr("transform", function (d) {
                    d.selected = false;
                    var d3SelectedNode = __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("." + _this.selectedNode.customClass.split(' ').join('.'));
                    _this.skillNodeHexagonBadge(d3SelectedNode, true);
                    _this.generateSkillNodeText(false);
                    _this.selectedNode = null;
                    d3SelectedNodeTranslate_1.x = parseInt(d3SelectedNodeTranslate_1.x) - 200;
                    _this.showSkills(false);
                    d3SelectedNode.attr('translateX', d3SelectedNodeTranslate_1.x);
                    return "translate(" + d3SelectedNodeTranslate_1.x + "," + d3SelectedNodeTranslate_1.y + ")";
                });
            }
            //Rotate Hexagons
            //Parameter: clicked node index in integer format
            //Callback is triggered after rotation ends
            this.rotateHexagons(6 - parseInt(clickedNode.attr("index")), function () {
                var clickedNodeTranslate = {
                    x: clickedNode.attr('translateX'),
                    y: clickedNode.attr('translateY')
                };
                clickedNode.attr("transform", function (d) {
                    d.selected = true;
                    _this.selectedNode = d;
                    _this.getSelectedNodeDetails(_this.selectedNode);
                    clickedNodeTranslate.x = parseInt(clickedNodeTranslate.x) + 200;
                    var d3SelectedNode = __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("." + _this.selectedNode.customClass.split(' ').join('.'));
                    _this.showSkills(true);
                    _this.generateSkillNodeText(true);
                    _this.skillNodeHexagonBadge(d3SelectedNode, false);
                    clickedNode.attr('translateX', clickedNodeTranslate.x);
                    return "translate(" + clickedNodeTranslate.x + "," + clickedNodeTranslate.y + ")";
                });
                _this.hexagonTransitionFinished = true;
            });
        }
    };
    HexagonComponent.prototype.skillNodeHexagonBadge = function (d3SelectedNode, shouldShow) {
        d3SelectedNode.selectAll(".badge-0").style("fill", "none");
        d3SelectedNode.selectAll(".badge.badge-0, .trapeze.badge-0").style("fill", shouldShow ? this.badgeColor.trapeze : "none");
        d3SelectedNode.selectAll(".text.badge-0").style("fill", shouldShow ? this.badgeColor.text : "none");
    };
    HexagonComponent.prototype.createOccupationAssociationsGraph = function () {
        var _this = this;
        //mainOccupation and customClasses associated
        this.occupation.customClass = "mainOccupation";
        var mainOccupationCoords = { x: 150, y: 200, radius: 70 };
        //associatedOccupation and customClasses associated
        this.classCommonToAllAssociatedOccupation = 'associated';
        this.associatedOccupations.forEach(function (occupation, index) {
            occupation.customClass = _this.classCommonToAllAssociatedOccupation + ' ' + 'node-' + index;
        });
        //console.log(this.occupation);
        var allOccupations = [this.occupation].concat(this.associatedOccupations);
        //Calculate first the 6 associated hexagon centers by using mainHexagonRadius + diff as radius
        //After finding the centers use mainHexagonRadius and associatedHexagonRadius to design the hexagons with d3
        var gapBetweenMainAndAssociatedHexagons = 20;
        var associatedHexagonRadius = 80;
        this.associatedOccupationsHexagonCoords = this.generateHexagonMesh(this.associatedOccupations, mainOccupationCoords, gapBetweenMainAndAssociatedHexagons, associatedHexagonRadius);
        var allOccupationsHexagonCoords = [mainOccupationCoords].concat(this.associatedOccupationsHexagonCoords);
        // this.svg.attr("transform","translate(" + this.margin.left + "," + this.margin.top + ")") 
        this.g = this.svg
            .append("g").attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
            .append("g").attr("transform", "translate(" + 103.47908500155617 + "," + 63.93163167161124 + ")scale(" + 0.6584848679816628 + ")");
        //Create lines for the given form
        this.createForm = __WEBPACK_IMPORTED_MODULE_4_d3__["h" /* line */]()
            .x(function (d) { return d.x; })
            .y(function (d) { return d.y; })
            .curve(__WEBPACK_IMPORTED_MODULE_4_d3__["a" /* curveLinearClosed */]);
        //Having the center pont of a hex and a radius, generate the 6 intersection points
        var hexDataGenerator = function (hexCoords) {
            var apothemPercentage = (Math.sqrt(3) / 2);
            return [
                { "x": hexCoords.radius * apothemPercentage + hexCoords.x, "y": -hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.radius * apothemPercentage + hexCoords.x, "y": hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.x, "y": hexCoords.y + hexCoords.radius },
                { "x": -hexCoords.radius * apothemPercentage + hexCoords.x, "y": hexCoords.radius / 2 + hexCoords.y },
                { "x": -hexCoords.radius * apothemPercentage + hexCoords.x, "y": -hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.x, "y": hexCoords.y - hexCoords.radius },
            ];
        };
        var rotationDeg = 30;
        var occupationNodes = this.g.selectAll(".node")
            .data(allOccupations)
            .enter()
            .append("g")
            .attr("class", function (d) {
            return "node-occupation " + d.customClass;
        });
        this.g.selectAll("." + this.classCommonToAllAssociatedOccupation).on("click", function (d) { return _this.associationNodeClick(d); });
        this.g.selectAll(".node-occupation")
            .data(allOccupationsHexagonCoords)
            .attr("translateX", 0)
            .attr("translateY", 0)
            .attr("index", function (d, i) { return i - 1; })
            .attr("coordsX", function (coords) { return coords.x; })
            .attr("coordsY", function (coords) { return coords.y; })
            .append("path")
            .attr("d", function (coords) { return _this.createForm(hexDataGenerator(coords)); })
            .attr("stroke", "red")
            .attr("fill", "rgba(255,0,0,0.4)");
        this.createBadges();
        this.createText(allOccupations, mainOccupationCoords);
        var coordXOfClickedAssociatedOccupation = this.associatedOccupationsHexagonCoords[0].x + 200; //200 is the same as on the rotateHexagon click should be global
        var distanceYBetweenSkillNodes = 50; //define as global variable
        // let distanceXBetweenSkillAndOccupationNode
        var apothemPercentage = (Math.sqrt(3) / 2);
        this.occupationSkillsAssociation = [
            //{ x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y - distanceYBetweenSkillNodes, occupation: { x: mainOccupationCoords.x, y: mainOccupationCoords.y } },
            { x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y, occupation: { x: mainOccupationCoords.x + apothemPercentage * mainOccupationCoords.radius, y: mainOccupationCoords.y } },
            { x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y, occupation: { x: coordXOfClickedAssociatedOccupation - apothemPercentage * associatedHexagonRadius, y: mainOccupationCoords.y } },
        ];
        this.skillNodes = [
            //{ x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y + -distanceYBetweenSkillNodes, type: 'skill' },
            //{ x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y + distanceYBetweenSkillNodes, type: 'skill' },
            { x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y, type: 'skill' },
        ];
        this.createLinksForSkillNodes(this.occupationSkillsAssociation);
        this.createSkillNodes(this.skillNodes);
        this.showSkills(false);
    };
    HexagonComponent.prototype.createBadges = function () {
        var _this = this;
        this.badgeColor = {
            "trapeze": "gray",
            "badge": "gray",
            "text": "black"
        };
        var hexagonBadges = this.generateBadgesMesh(this.associatedOccupationsHexagonCoords);
        var _loop_1 = function (index) {
            var badgeIndex = (index + 3) % 6;
            __WEBPACK_IMPORTED_MODULE_4_d3__["l" /* selectAll */]("." + this_1.classCommonToAllAssociatedOccupation)
                .data(hexagonBadges)
                .append("path")
                .attr("d", function (d) { return _this.createForm(d[index].trapeze); })
                .attr("index", badgeIndex)
                .attr("class", "trapeze badge-" + badgeIndex)
                .style("fill", function (d, i) { return i == badgeIndex ? _this.badgeColor.trapeze : "none"; });
            __WEBPACK_IMPORTED_MODULE_4_d3__["l" /* selectAll */]("." + this_1.classCommonToAllAssociatedOccupation)
                .data(hexagonBadges)
                .append("circle")
                .attr("cx", function (d) { return d[index].badge.x; })
                .attr("cy", function (d) { return d[index].badge.y; })
                .attr("r", 14)
                .attr("class", "badge badge-" + badgeIndex)
                .style("fill", function (d, i) { return i == badgeIndex ? _this.badgeColor.badge : "none"; });
            __WEBPACK_IMPORTED_MODULE_4_d3__["l" /* selectAll */]("." + this_1.classCommonToAllAssociatedOccupation)
                .data(hexagonBadges)
                .append("text")
                .attr("x", function (d) { return d[index].badge.x; })
                .attr("y", function (d) { return d[index].badge.y + 2; })
                .attr("text-anchor", "middle")
                .text(function (d, i) { return (Math.round(_this.associatedOccupations[i].score * 100 / (_this.occupation.skillsScore + _this.associatedOccupations[i].skillsScore)) + "%"); })
                .attr("font-family", "sans-serif")
                .attr("font-size", "10px")
                .attr("class", "text badge-" + badgeIndex)
                .style("fill", function (d, i) { return i == badgeIndex ? _this.badgeColor.text : "none"; });
        };
        var this_1 = this;
        for (var index = 0; index < 6; index++) {
            _loop_1(index);
        }
    };
    HexagonComponent.prototype.createText = function (allOccupations, mainOccupationCoords) {
        this.g.selectAll(".node-occupation")
            .data(allOccupations)
            .append("text")
            .attr("class", "node-occupation-text")
            .attr("x", function (d) {
            var coordsX = __WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("." + d.customClass.split(' ').join('.')).attr('coordsX');
            return parseInt(coordsX); //- mainOccupationCoords.radius + 10;
        })
            .attr("y", function (d) {
            return parseInt(__WEBPACK_IMPORTED_MODULE_4_d3__["k" /* select */]("." + d.customClass.split(' ').join('.')).attr('coordsY'));
        })
            .attr("text-anchor", "middle")
            .text(function (d) { return d.shortDescription; })
            .attr("font-family", "sans-serif")
            .attr("font-size", "10px")
            .attr("fill", "black")
            .attr("inline-size", "10px");
        // let wrap = d3Wrap.textwrap()
        //     // wrap to 480 x 960 pixels
        //     .bounds({height: 80, width: 50})
        //     // // pad by an additional 10 pixels
        //     // .padding(10);
        // this.g.selectAll(".node-occupation-text")
        //     .call(wrap);
    };
    HexagonComponent.prototype.createLinksForSkillNodes = function (occupationSkillsAssociation) {
        this.g.selectAll(".link")
            .data(occupationSkillsAssociation)
            .enter().append("path")
            .attr("class", "link")
            .attr("d", function (d) {
            return d.occupation != null ? "M" + d.x + "," + d.y
                + "C" + d.x + "," + (d.y + d.occupation.y) / 2
                + " " + d.occupation.x + "," + (d.y + d.occupation.y) / 2
                + " " + d.occupation.x + "," + d.occupation.y : null;
        });
    };
    HexagonComponent.prototype.createSkillNodes = function (skillNodes) {
        this.g.selectAll(".skillNodes")
            .data(skillNodes)
            .enter()
            .append("g")
            .attr("class", "skillBadge")
            .attr("transform", function (d) {
            return "translate(" + d.x + "," + d.y + ")";
        })
            .append("circle")
            .attr("r", 30)
            .attr("class", "skillNodes");
        this.g.selectAll(".skillBadge")
            .append("text")
            .attr("y", 2)
            .attr("class", "skillBadgeText")
            .attr("text-anchor", "middle")
            .attr("font-family", "sans-serif")
            .attr("font-size", "10px")
            .attr("fill", "black");
    };
    HexagonComponent.prototype.generateSkillNodeText = function (shouldShow) {
        var _this = this;
        if (shouldShow)
            this.g.selectAll(".skillBadgeText")
                .text(function () { return (Math.round(_this.selectedNode.score * 100 / (_this.occupation.skillsScore + _this.selectedNode.skillsScore)) + "%"); });
        else
            this.g.selectAll(".skillBadgeText")
                .text("");
    };
    HexagonComponent.prototype.showSkills = function (show) {
        this.g.selectAll(".skillNodes").transition().style("fill", show ? "grey" : "none");
        this.g.selectAll(".link").transition().style("stroke", show ? "grey" : "none");
    };
    HexagonComponent.prototype.getAssociatedOccupationsSuccess = function (response) {
        console.log(response);
        this.associatedOccupations = response.data;
        this.initGraph();
    };
    HexagonComponent.prototype.getAssociatedOccupationsError = function (error) {
    };
    return HexagonComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('occupation'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__interfaces_Occupation__["a" /* Occupation */])
], HexagonComponent.prototype, "occupation", void 0);
HexagonComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'hexagon-component',template:/*ion-inline-start:"E:\boostrs\ngAdmin\src\pages\hexagon\hexagon.html"*/'<ion-grid>\n\n    <ion-row>\n\n        <ion-col col col-lg-3 col-12>\n\n            <ion-row class="occupationDetails" [hidden]="showSkillButtons.common || showSkillButtons.diff">\n\n                <ion-row>\n\n                    <ion-col class="occupationLabel" col col-12>\n\n                        Short Description:\n\n                    </ion-col>\n\n                    <ion-col col col-12>\n\n                        {{selectedOccupation && selectedOccupation.shortDescription ? selectedOccupation.shortDescription : \'\'}}\n\n                    </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                    <ion-col class="occupationLabel" col col-12>\n\n                        Code ISCO Label:\n\n                    </ion-col>\n\n                    <ion-col col col-12>\n\n                        {{selectedOccupation && selectedOccupation.codeISCOLabel ? selectedOccupation.codeISCOLabel : \'\'}}\n\n                    </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                    <ion-col class="occupationLabel" col col-12>\n\n                        Code ISCO:\n\n                    </ion-col>\n\n                    <ion-col col col-12>\n\n                        {{selectedOccupation && selectedOccupation.codeISCO ? selectedOccupation.codeISCO : \'\'}}\n\n                    </ion-col>\n\n                </ion-row>\n\n                <ion-row>\n\n                    <ion-col class="occupationLabel" col col-12>\n\n                        Long Description:\n\n                    </ion-col>\n\n                    <ion-col col col-12>\n\n                        {{selectedOccupation && selectedOccupation.longDescription ? selectedOccupation.longDescription : \'\'}}\n\n                    </ion-col>\n\n                </ion-row>\n\n                <!-- <ion-row [hidden]="!selectedNode && !selectedNodeDetailsDone" col col-12>\n\n                    <ion-col col col-12 (click)="showCommonSkills()">\n\n                        Click to show Common Skills\n\n                    </ion-col>\n\n                    <ion-col col col-12 (click)="showDiffSkills()">\n\n                        Click to show Different Skills\n\n                    </ion-col>\n\n                </ion-row> -->\n\n            </ion-row>\n\n\n\n            <ion-scroll [hidden]="!showSkillButtons.common" style="width:100%;height:100vh" scrollY="true">\n\n                <ion-row (click)="goBack()">Go Back</ion-row>\n\n                <ion-list scroll="true">\n\n                    <ion-item>\n\n                        <div>commonSkills1</div>\n\n                        <div>commonSkills2</div>\n\n                        <div>commonSkills3</div>\n\n                        <div>commonSkills4</div>\n\n                        <div>commonSkills1</div>\n\n                        <div>commonSkills2</div>\n\n                        <div>commonSkills3</div>\n\n                        <div>commonSkills4</div>\n\n                        <div>commonSkills1</div>\n\n                        <div>commonSkills2</div>\n\n                        <div>commonSkills3</div>\n\n                        <div>commonSkills4</div>\n\n                        <div>commonSkills1</div>\n\n                        <div>commonSkills2</div>\n\n                        <div>commonSkills3</div>\n\n                        <div>commonSkills4</div>\n\n                        <div>commonSkills1</div>\n\n                        <div>commonSkills2</div>\n\n                        <div>commonSkills3</div>\n\n                        <div>commonSkills4</div>\n\n                        <div>commonSkills1</div>\n\n                        <div>commonSkills2</div>\n\n                        <div>commonSkills3</div>\n\n                        <div>commonSkills4</div>\n\n                    </ion-item>\n\n                </ion-list>\n\n            </ion-scroll>\n\n            <ion-scroll [hidden]="!showSkillButtons.diff" style="width:100%;height:100vh" scrollY="true">\n\n                <ion-row (click)="goBack()">Go Back</ion-row>\n\n                <ion-list scroll="true">\n\n                    <ion-item>\n\n                        <div>diffSkills1</div>\n\n                        <div>diffSkills2</div>\n\n                        <div>diffSkills3</div>\n\n                        <div>diffSkills4</div>\n\n                        <div>diffSkills1</div>\n\n                        <div>diffSkills2</div>\n\n                        <div>diffSkills3</div>\n\n                        <div>diffSkills4</div>\n\n                        <div>diffSkills1</div>\n\n                        <div>diffSkills2</div>\n\n                        <div>diffSkills3</div>\n\n                        <div>diffSkills4</div>\n\n                        <div>diffSkills1</div>\n\n                        <div>diffSkills2</div>\n\n                        <div>diffSkills3</div>\n\n                        <div>diffSkills4</div>\n\n                        <div>diffSkills1</div>\n\n                        <div>diffSkills2</div>\n\n                        <div>diffSkills3</div>\n\n                        <div>diffSkills4</div>\n\n                        <div>diffSkills1</div>\n\n                        <div>diffSkills2</div>\n\n                        <div>diffSkills3</div>\n\n                        <div>diffSkills4</div>\n\n                    </ion-item>\n\n                </ion-list>\n\n            </ion-scroll>\n\n        </ion-col>\n\n        <ion-col col col-lg-9 col-12 class="svgComponent">\n\n            <svg preserveAspectRatio="none" class="graph" [attr.height.px]="this.platform.height() * 80/100"\n\n                [attr.width.px]="this.platform.width() < 960 ? this.platform.width() * 95 / 100 : this.platform.width() * 70/100">\n\n            </svg>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-grid>'/*ion-inline-end:"E:\boostrs\ngAdmin\src\pages\hexagon\hexagon.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__services_OccupationsService__["a" /* OccupationsService */], __WEBPACK_IMPORTED_MODULE_5__services_SkillsService__["a" /* SkillsService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_OccupationsService__["a" /* OccupationsService */], __WEBPACK_IMPORTED_MODULE_5__services_SkillsService__["a" /* SkillsService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Platform */]])
], HexagonComponent);

//# sourceMappingURL=hexagon.js.map

/***/ }),

/***/ 755:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Occupation; });
var Occupation = (function () {
    function Occupation() {
    }
    return Occupation;
}());

//# sourceMappingURL=Occupation.js.map

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//let apiUrl = 'http://localhost:9000';
var apiUrl = 'https://boostrs-expressapi.herokuapp.com';
//let apiUrl = "http://165.227.233.196:9000";
/* harmony default export */ __webpack_exports__["a"] = (apiUrl);
//# sourceMappingURL=apiUrl.js.map

/***/ })

},[393]);
//# sourceMappingURL=main.js.map