import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import * as MarkerClusterer from 'node-js-marker-clusterer';
import 'rxjs/add/operator/map';

declare var google: any;
/*
  Generated class for the GoogleMapsClusterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GoogleMapsClusterProvider {
  markerCluster: any;
  locations: any;

  constructor(public http: Http) {
    console.log('Hello GoogleMapsCluster Provider');

    this.locations = [
      { lat: -31.563910, lng: 147.154312, mainOccupation: "Advertising Manager", codeISCO: "ISCO 3" },
      { lat: 52.486243, lng: -1.890401, mainOccupation: "Public finance accountant", country: "England", city: "Birmingham", codeISCO: 2411 },
      { lat: 51.507351, lng: -0.127758, mainOccupation: "Tax advisor", country: "England", city: "London", codeISCO: 2411 },
      { lat: 53.480759, lng: -2.242631, mainOccupation: "Design engineer", country: "England", city: "Manchester", codeISCO: 2149 },
      { lat: 51.517371, lng: -0.137778, mainOccupation: "Audit supervisor", country: "England", city: "London", codeISCO: 2411 },
      { lat: 51.547381, lng: -0.117718, mainOccupation: "HR manager", country: "England", city: "London", codeISCO: 1212 },
      { lat: 51.527391, lng: -0.127728, mainOccupation: "Data scientist", country: "England", city: "London", codeISCO: 2511 },
      { lat: 43.610769, lng: 3.876716, mainOccupation: "Tehnical director", country: "France", city: "Montpeiller", codeISCO: 2654 },
      { lat: 48.826614, lng: 2.372222, mainOccupation: "Specialised Doctor", country: "France", city: "Paris", codeISCO: 2212 },
      { lat: 45.764043, lng: 4.835659, mainOccupation: "Financial broker", country: "France", city: "Lyon", codeISCO: 3311 },
      { lat: 47.218371, lng: -1.553621, mainOccupation: "Company directors", country: "France", city: "Nantes", codeISCO: 2654 },
      { lat: 48.856614, lng: 2.352222, mainOccupation: "Civil service administrative officers", country: "France", city: "Paris", codeISCO: 2422 },
      { lat: 43.552847, lng: 7.017369, mainOccupation: "Airplane transport pilot", country: "France", city: "Cannes", codeISCO: 3153 },
      { lat: 52.560007, lng: 13.424954, mainOccupation: "Technical director", country: "Germany", city: "Berlin", codeISCO: 2654 },
      { lat: 52.550007, lng: 13.444954, mainOccupation: "Government minister", country: "Germany", city: "Berlin", codeISCO: 1111 },
      { lat: 48.135125, lng: 11.581980, mainOccupation: "Corporate Investment banker", country: "Germany", city: "Munchen", codeISCO: 2412 },
      { lat: 52.510007, lng: 13.424954, mainOccupation: "Sales manager", country: "Germany", city: "Berlin", codeISCO: 1221 },
      { lat: 50.110922, lng: 8.682127, mainOccupation: "Medical laboratory manager", country: "Germany", city: "Frankfurt", codeISCO: 1349 },
      { lat: 53.551085, lng: 9.993682, mainOccupation: "Lawyer", country: "Germany", city: "Hamburg", codeISCO: 2611 },
      { lat: 39.469907, lng: -0.376288, mainOccupation: "Business Economics Researcher", country: "Spain", city: "Valencia", codeISCO: 2631 },
      { lat: 41.395064, lng: 2.123403, mainOccupation: "Operations manager", country: "Spain", city: "Barcelona", codeISCO: 1321 },
      { lat: 41.335064, lng: 2.153403, mainOccupation: "Chief Fire Officer", country: "Spain", city: "Barcelona", codeISCO: 1349 },
      { lat: 40.416775, lng: -3.703790, mainOccupation: "Talent Agent", country: "Spain", city: "Madrid", codeISCO: 3339 },
      { lat: 40.416775, lng: -3.703790, mainOccupation: "Marketing Manager", country: "Spain", city: "Madrid", codeISCO: 1221 },
      { lat: 41.325064, lng: 2.103403, mainOccupation: "Telecommunications Manager", country: "Spain", city: "Barcelona", codeISCO: 1330 },
    ];
  }

  addCluster(map) {
    console.log(map, google.maps);
    if (google.maps) {
      console.log('Init clusters');
      var infowindow = new google.maps.InfoWindow();

      //Convert locations into array of markers
      let markers = this.locations.map((location) => {
        return new google.maps.Marker({
          position: location
        });
      });

      markers.forEach((marker, index) => {
        let self = this;
        google.maps.event.addListener(marker, 'click', function () {
          infowindow.setContent('<div><strong>Main Occupation</strong><br>' +
            'Occupation Name: ' + self.locations[index].mainOccupation + '<br>' +
            'Code ISCO: ' + self.locations[index].codeISCO + '</div>');
          infowindow.open(map, this);
        });
      });

      this.markerCluster = new MarkerClusterer(map, markers, { imagePath: 'assets/images/m' });

    } else {
      console.warn('Google maps needs to be loaded before adding a cluster');
    }

  }
}
