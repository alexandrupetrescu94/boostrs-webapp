import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

/*
  Generated class for the ConnectivityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConnectivityProvider {

  onDevice: boolean;
  connection: boolean;

  constructor(public platform: Platform, private network: Network) {
    this.onDevice = this.platform.is('cordova');
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log('offline');
      this.connection = false;
    });
    let connectSubscription = this.network.onConnect().subscribe(() => {
      setTimeout(() => {
        console.log('online');
        this.connection = true;
      }, 3000);
    });
  }

  isOnline(): boolean {
    if (this.onDevice && this.connection) {
      return true
    } else {
      return navigator.onLine;
    }
  }

  isOffline(): boolean {
    if (this.onDevice && this.connection) {
      return true;
    } else {
      return !navigator.onLine;
    }
  }

  watchOnline(): Observable<any> {
    return this.network.onConnect();
  }

  watchOffline(): Observable<any> {
    return this.network.onDisconnect();
  }

}
