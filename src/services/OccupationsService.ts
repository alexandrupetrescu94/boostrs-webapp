import { Injectable } from '@angular/core';
import { Occupation } from '../interfaces/Occupation';
import { Category } from '../interfaces/Category';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import apiUrl from './apiUrl';

interface OccupationResponseModel {
    occupations: Occupation[];
};

interface CategoriesResponseModel {
    categories: Category[];
};

interface HistogramResponseModel {
    histogramArray: any[];
};

@Injectable()
export class OccupationsService {
    private occupationUrl = `${apiUrl}/api/occupations`;

    constructor(private http: HttpClient) {}

    get(): Observable<OccupationResponseModel> {
        return this.http.get<OccupationResponseModel>(this.occupationUrl);
    }

    getOccupationAssociationsHistogram(): Observable<HistogramResponseModel> {
        return this.http.get<HistogramResponseModel>(`${this.occupationUrl}/associationsHistogram`);
    } 

    getOccupationsForMinorCategory(minorCodeISCO): Observable<OccupationResponseModel> {
        return this.http.post<OccupationResponseModel>(
            `${this.occupationUrl}/minorCodeISCO`,
            {
                minorCodeISCO
            }
        )
    }

    getOccupationByCodeISCO(codeISCO): Observable<OccupationResponseModel> {
        return this.http.post<OccupationResponseModel>(
            `${this.occupationUrl}/codeISCO`,
            {
                codeISCO
            }
        );
    }

    getCodeISCODescription(codeISCOArray): Observable<CategoriesResponseModel> {
        return this.http.post<CategoriesResponseModel>(
            `${this.occupationUrl}/findCodeISCO`,
            {
                codeISCOArray
            }
        )
    }

    getOccupationByName(occupationName): Observable<OccupationResponseModel> {
        return this.http.post<OccupationResponseModel>(`${this.occupationUrl}/name`,
            {
                occupationName
            }
        );
    }

    getAssociatedOccupations(occupationIRI): Observable<OccupationResponseModel> {
        return this.http.get<OccupationResponseModel>(this.occupationUrl + `/${occupationIRI}/associatedOccupations/`);
    }

    getOccupationDetails(occupationIRI): Observable<OccupationResponseModel> {
        return this.http.get<OccupationResponseModel>(this.occupationUrl + `/iri/${occupationIRI}/`)
    }

    getOccupationCategories(occupationCategory, codeISCO): Observable<CategoriesResponseModel> {
        return this.http.post<CategoriesResponseModel>(
            this.occupationUrl + `/categories/`, 
            {
                occupationCategory: occupationCategory,
                codeISCO
            }
        );
    }
}