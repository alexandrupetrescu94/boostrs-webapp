import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private storage: Storage) {}
 
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if ( !req.body || (!req.body.hasOwnProperty('withToken') || req.body.withToken) ) {
      const authReq = req.clone({headers: req.headers.set('x-access-token', localStorage.getItem("token"))});
      return next.handle(authReq);
    } else {
      return next.handle(req);
    }
  }
}