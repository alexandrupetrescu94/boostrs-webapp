import { Injectable } from '@angular/core';
import { Skill } from '../interfaces/Skill';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import apiUrl from './apiUrl';

interface SkillsResponseModel {
    skills: Skill[];
}

@Injectable()
export class SkillsService {
    private skillsUrl = `${apiUrl}/api/skills`;

    constructor(private http: HttpClient, private storage: Storage) {}
    
    get(): Observable<SkillsResponseModel> {
        return this.http.get<SkillsResponseModel>(this.skillsUrl);
    }

    getFiveSkills(): Observable<SkillsResponseModel> {
        return this.http.get<SkillsResponseModel>(this.skillsUrl + "/fiveskills");
    }

    getCommonSkills(occupationIRI1, occupationIRI2): Observable<SkillsResponseModel> {
        if (occupationIRI1 > occupationIRI2) {
            let aux = occupationIRI1;
            occupationIRI1 = occupationIRI2;
            occupationIRI2 = aux;
        }
            
        return this.http.post<SkillsResponseModel>(
            `${this.skillsUrl}/occupationsCommonSkills`,
            {
                occupationIRI1,
                occupationIRI2
            }
        );
    }
}