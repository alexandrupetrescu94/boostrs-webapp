import { Injectable } from '@angular/core';
import { Association } from '../interfaces/Association';
import { Http, Headers }    from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import apiUrl from './apiUrl';

@Injectable()
export class AssociationsService {
    private associationUrl = `${apiUrl}/api/associations`;

    constructor(private http: Http, private httpClient: HttpClient) {
        
    }
    
    get(): Promise<Association[]> {
        return this.http.get(this.associationUrl, {headers: new Headers({'x-access-token': localStorage.getItem('token')}) })
          .toPromise()
          .then(response => { return response.json().data as Association[] })
          .catch(this.handleError);
    } 
    
    postOcuppationSkills(occupationIRI: string): Promise<Association[]> {
        return this.http.post(
            this.associationUrl + '/ocupationSkills',
            {occupationIRI: occupationIRI},
            {headers: new Headers({'x-access-token': localStorage.getItem('token')}) }
        )
          .toPromise()
          .then(response => { return response.json().data as Association[] })
          .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}