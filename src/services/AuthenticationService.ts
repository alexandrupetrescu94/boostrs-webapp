import { Injectable } from '@angular/core';
import { HttpClient }    from '@angular/common/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import apiUrl from './apiUrl';

export class LoginModel {
    username: string;
    password: string;
}

export class LoginResponseModel {
    success: boolean;
    message: string;
    token: string;
}

@Injectable()
export class AuthenticationService {
    private authenticationUrl = `${apiUrl}/authenticate`;

    constructor(private http: HttpClient) {}
    
    post(loginForm: LoginModel): Observable<LoginResponseModel> {
        return this.http.post<LoginResponseModel>(this.authenticationUrl, {name: loginForm.username, password: loginForm.password, withToken: false})
    }
}