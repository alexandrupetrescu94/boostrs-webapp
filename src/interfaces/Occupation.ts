import { pointCoords } from './PointCoords';

export class Occupation {
    _id: string;
    IRI: string;
    codeISCO: string;
    codeISCOLabel: string;
    shortDescription: string;
    longDescription: string;
    skillsScore: number;
    selected?: boolean;
    main?: boolean;
    coords?: pointCoords;
    customClass?: string;
    score?: number;
    group?: string;
    inMinorISCO?: boolean;
    id?: string;
    index?: number;
}