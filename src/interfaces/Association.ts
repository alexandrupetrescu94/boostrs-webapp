export class Association {
    _id: string;
    objectIRI: string;
    targetIRI: string;
    relationshipType: string;
}