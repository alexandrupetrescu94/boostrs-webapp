export type pointCoords = {
    x: number;
    y: number;
    radius: number;
}