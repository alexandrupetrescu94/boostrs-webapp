import {Occupation} from './Occupation';

export class Skill {
    _id: string;
    IRI: string;
    reuseType: string;
    nrOfAssociatedOccupations: number; 
    nrOfAssociatedOccupationsEssential: number;
    occupation: Occupation;
}