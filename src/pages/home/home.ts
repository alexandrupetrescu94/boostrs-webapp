import { Component } from '@angular/core';
import { NavController, NavParams, Platform, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/forkJoin';

import { OccupationsService } from '../../services/OccupationsService';
import { SkillsService } from '../../services/SkillsService';
import { AssociationsService } from '../../services/AssociationsService';
import { Skill } from '../../interfaces/Skill';
import { Occupation } from '../../interfaces/Occupation';
import { Association } from '../../interfaces/Association';
import * as d3 from 'd3';
import * as d3Selection from 'd3-selection';
import * as d3Scale from "d3-scale";
import * as d3Shape from "d3-shape";
import * as d3Array from "d3-array";
import * as d3Axis from "d3-axis";

//import { NeoVis } from "../../assets/lib/neovis/src/neovis";
declare let NeoVis: any;

import { graphJson } from './graph'
import { Observable } from 'rxjs/Observable';
import { ListPage } from '../list/list';
import { Category } from '../../interfaces/Category';

export type IDType = { id: any };

interface OccupationResponseModel {
  occupations: Occupation[];
};
interface Legend {
  codeISCO: string;
  groupColor?: string;
  description?: string;
}
interface ResetGraphButton {
  codeISCO: boolean;
  node: boolean;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [OccupationsService, SkillsService, AssociationsService]
})
export class HomePage {
  uniqueISCODescription: any;
  associatedOccupationCategorization: any;
  showResetGraphButton: ResetGraphButton;
  uniqueISCO: Legend[];
  ISCODescription: any;
  searchInput: any;
  public occupations: Occupation[];
  public skills: Skill[];
  public associations: Association[];
  public svg: any;
  public simulation: any;
  public width: any;
  public height: any;
  name: string;
  color;
  link;
  node;
  svgGraph;
  occupationNodes;
  skillsNodes;
  associationsLinks;
  viz: any;
  loading: any;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    private occupationsService: OccupationsService,
    private associationsService: AssociationsService,
    private skillsService: SkillsService,
    private storage: Storage,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
  ) {
    this.showResetGraphButton = { codeISCO: false, node: false };
    this.svgGraph = {}
    this.svgGraph.nodes = [];
    this.svgGraph.links = [];
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  ionViewWillEnter() {
    this.presentLoadingDefault();
    this.showResetGraphButton = { codeISCO: false, node: false };
    this.searchInput = this.navParams.get('searchInput');
    this.occupationsService.getOccupationsForMinorCategory(this.searchInput).subscribe(
      result => this.getOccupationsForMinorCategorySuccess(result),
      error => this.getOccupationsForMinorCategoryError(error),
      () => { }
    )
  }

  goBack() {
    this.resetGraphHighlighting();
    let searchInput = this.searchInput;
    this.navCtrl.setRoot(ListPage, {
      searchInput
    });
  }

  tryRender() {
    this.svg = d3.select("svg");

    var width = +this.svg.attr("width");
    var height = +this.svg.attr("height");

    this.color = d3.scaleOrdinal(d3.schemeCategory20);

    this.simulation = d3.forceSimulation()
      .force("link", d3.forceLink().id(function (d: IDType) { return d.id; }))
      .force("charge", d3.forceManyBody().strength(-50).distanceMax(600).distanceMin(1))
      .force("center", d3.forceCenter(width / 2, height / 2));

    this.render(this.svgGraph);
  }

  ticked() {
    this.link
      .attr("x1", function (d) { return d.source.x; })
      .attr("y1", function (d) { return d.source.y; })
      .attr("x2", function (d) { return d.target.x; })
      .attr("y2", function (d) { return d.target.y; });

    this.node
      .attr("cx", function (d) { return d.x; })
      .attr("cy", function (d) { return d.y; })
      .attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")"; });
  }

  render(graph) {
    this.node = this.svg.append("g")
      .attr("class", "nodes")
      .selectAll(".node")
      .data(graph.nodes)
      .enter().append("g")
      .attr("class", (d) => `node gNodes gNodes-${d.codeISCO} gNodes-${d.id}`)
      .on("click", (d: Occupation) => {console.log('highlight'); this.highlightNode(d)})
      .call(d3.drag()
        .on("start", (d) => { return this.dragstarted(d) })
        .on("drag", (d) => { return this.dragged(d) })
        .on("end", (d) => { return this.dragended(d) }))

    this.node.append("circle")
      .attr("r", (node: Occupation) => { return node.inMinorISCO ? 15 : 10; })
      .attr("class", (d) => `circles circles-${d.codeISCO} circles-${d.id}`)
      .attr("fill", (d) => {
        let groupColor = this.color(d.group);
        d.groupColor = groupColor;
        this.uniqueISCO[this.uniqueISCO.findIndex((item) => item.codeISCO === d.codeISCO)].groupColor = groupColor;
        return groupColor
      })

    this.node.append("text")
      .text(function (d) { return d.shortDescription; });

    this.link = this.svg.append("g")
      .attr("class", "links")
      .selectAll("line")
      .data(graph.links)
      .enter()
      .append("line")
      .attr("class", (d) => `link link-${d.source} link-${d.target}`)
      .attr("stroke-width", function (d) { return 1; });

    this.node.append("title")
      .text(function (d: Occupation) { return `Name: ${d.shortDescription}, ISCO: ${d.codeISCO}`; });

    this.simulation
      .nodes(graph.nodes)
      .on("tick", () => { return this.ticked() });

    this.simulation.force("link")
      .links(graph.links);

    //add more negative attraction between nodes
    //add histroy in which you explain color - isco + name of code isco

    this.svg.call(d3.zoom().on("zoom", () => {
      this.svg.selectAll(".links, .nodes").attr("transform", d3.event.transform)
    }));

  }

  highlightNode(occupation: Occupation) {
    if (!occupation.inMinorISCO) return;
    this.showResetGraphButton.codeISCO = false;
    this.svg.selectAll(".gNodes").attr("opacity", 0);
    this.svg.selectAll(".circles").attr("fill", "gray");
    this.svg.selectAll(".link").attr("opacity", 0);

    this.svg.select(`.gNodes-${occupation.id}`).attr("opacity", 1);
    this.svg.select(`.circles-${occupation.id}`).attr("fill", d => d.groupColor);

    let occupationAssociationsCircles = [];
    this.svgGraph.links.forEach((d) => {
      if (occupation.id == d.source.id) {
        occupationAssociationsCircles.push(d.target.id);
      }
      if (occupation.id == d.target.id) {
        occupationAssociationsCircles.push(d.source.id);
      }
    });

    occupationAssociationsCircles.forEach(occupationID => {
      this.svg.select(`.gNodes-${occupationID}`).attr("opacity", 1);
      this.svg.select(`.circles-${occupationID}`).attr("fill", d => d.groupColor)
    });

    this.svg.selectAll(`.link-${occupation.id}`).attr("opacity", 1);
    this.showResetGraphButton.node = true;

  }

  highlightISCO(codeISCO) {
    if (this.showResetGraphButton.node) return;
    this.svg.selectAll(".gNodes").attr("opacity", 0.3);
    this.svg.selectAll(".circles").attr("fill", "gray");

    this.svg.selectAll(`.gNodes-${codeISCO}`).attr("opacity", 1);
    this.svg.selectAll(`.circles-${codeISCO}`).attr("fill", d => d.groupColor);
    this.showResetGraphButton.codeISCO = true;
  }

  resetGraphHighlighting() {
    this.uniqueISCO.forEach((u: Legend) => {
      this.svg.selectAll(`.gNodes-${u.codeISCO}`).attr("opacity", 1);
      this.svg.selectAll(`.circles-${u.codeISCO}`).attr("fill", u.groupColor);
    });
    if (this.showResetGraphButton.node) {
      this.svg.selectAll(".link").attr("opacity", 1);
      this.showResetGraphButton.node = false;
    }
    this.showResetGraphButton.codeISCO = false;
  }

  dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }

  dragended(d) {
    if (!d3.event.active) this.simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
  }

  dragstarted(d) {
    if (!d3.event.active) this.simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
  }

  getOccupationsForMinorCategorySuccess(result: { occupations: Occupation[] }) {
    this.occupations = result.occupations.map((occupation: Occupation, index) => { occupation.index = index; return occupation });
    let occupationAssociationsObservable: Observable<OccupationResponseModel>[] = [];
    this.occupations.forEach((occupation: Occupation) => {
      occupationAssociationsObservable.push(this.occupationsService.getAssociatedOccupations(occupation.IRI));
    })

    Observable.forkJoin(occupationAssociationsObservable)
      .subscribe(
        result => this.getOccupationAndAssociations(result),
        err => console.log(err),
        () => { }
      );
  }

  getOccupationAndAssociations(responses) {
    //this.associatedOccupationCategorization = responses;
    let associatedOccupations = [].concat.apply([], responses.map((r) => r.data));

    let uniqueOccupations = this.occupations.concat(associatedOccupations).filter((occupation: Occupation, index, self) => {
      return self.findIndex((occ: Occupation) => occ.IRI === occupation.IRI) === index;
    });

    this.uniqueISCO = this.occupations.concat(associatedOccupations).filter((occupation: Occupation, index, self) => {
      return self.findIndex((occ: Occupation) => occ.codeISCO === occupation.codeISCO) === index;
    }).map((occupation: Occupation) => { return { codeISCO: occupation.codeISCO } }).sort((a: Legend, b: Legend) => a.codeISCO > b.codeISCO ? 1 : -1);

    this.occupationsService.getCodeISCODescription(this.uniqueISCO.map(u => u.codeISCO)).subscribe(
      response => this.getCodeISCODescriptonSuccess(response, uniqueOccupations, responses),
      error => this.getCodeISCODescriptionError(error),
      () => { }
    );

  }

  getCodeISCODescriptonSuccess(response, uniqueOccupations, responses) {
    let responseArray = response.categories;
    responseArray.sort((a, b) => a.codeISCO > b.codeISCO ? 1 : -1).forEach((category, index) => {
      this.uniqueISCO[index].description = category.description;
    });
    this.svgGraph.nodes = uniqueOccupations.map((uOcc: Occupation, index) => { uOcc.id = uOcc._id; uOcc.group = uOcc.codeISCO; uOcc.inMinorISCO = index < 50; return uOcc });
    this.svgGraph.links = [];
    this.svgGraph.links = [].concat.apply([], responses.map((r, index) => {
      let mainOccupation = this.occupations[index];
      return r.data.map((occupation: Occupation) => {
        return { target: occupation._id, source: mainOccupation._id, "value": occupation.score };
      });
    }));
    this.loading.dismiss();
    this.tryRender();
  }

  getCodeISCODescriptionError(error) {

  }

  draw() {
    var config = {
      container_id: "viz",
      encrypted: "ENCRYPTION_ON",
      server_url: "bolt://hobby-ldadcoiekhacgbkedkhlopal.dbs.graphenedb.com:24786",
      server_user: "expressapi",
      server_password: "b.b7l60l7o8XlW.8bxIHbp4bZ6P1ViN",
      labels: {
        "Occupation": {
          "name": "name",
          "codeISCO": "codeISCO",
          "id": "id"
        }
      },
      relationships: {
        "RELTYPE": {
        }
      },
      initial_cypher: "MATCH p=()-[r:RELTYPE]->() RETURN p LIMIT 25"
    };
    this.viz = new NeoVis.default(config);
    this.viz.render();
    console.log(this.viz);
  }

  getOccupationsForMinorCategoryError(error) {
    console.log(error);
  }

}
