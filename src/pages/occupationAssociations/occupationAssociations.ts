import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { OccupationsService } from '../../services/OccupationsService';

import { Occupation } from '../../interfaces/Occupation';
import { pointCoords } from '../../interfaces/PointCoords';
import { ListPage } from '../list/list';

@Component({
  selector: 'page-occupationAssociations',
  templateUrl: 'occupationAssociations.html',
  providers: [OccupationsService]
})
export class OccupationAssociationsPage {
  searchInput: any;
  occupation: Occupation;

  ionViewWillEnter() {
    this.occupation = this.navParams.get('occupation');
    this.searchInput = this.navParams.get('searchInput');
  }

  constructor(public navCtrl: NavController,  public navParams: NavParams, private occupationsService: OccupationsService) {
  }

  goBack() {
    let searchInput = this.searchInput;
    this.navCtrl.setRoot(ListPage, {
      searchInput
    });
  }

}