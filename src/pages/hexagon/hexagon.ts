import { Component, Input } from '@angular/core';
import { Occupation } from '../../interfaces/Occupation';
import { pointCoords } from '../../interfaces/PointCoords';
import { Platform } from 'ionic-angular';

import { OccupationsService } from '../../services/OccupationsService';

import * as d3 from 'd3';
import * as d3Selection from 'd3-selection';
import * as d3Scale from "d3-scale";
import * as d3Zoom from "d3-zoom";
import * as d3Wrap from "d3-textwrap";
import { SkillsService } from '../../services/SkillsService';


@Component({
    selector: 'hexagon-component',
    templateUrl: 'hexagon.html',
    providers: [OccupationsService, SkillsService]
}) export class HexagonComponent {
    g: d3Selection.Selection<d3Selection.BaseType, {}, HTMLElement, any>;
    svg: d3Selection.Selection<d3Selection.BaseType, {}, HTMLElement, any>;
    badgeColor: { "trapeze": string; "badge": string; "text": string; };
    hexagonTransitionFinished: boolean;
    classCommonToAllAssociatedOccupation: string;
    occupationSkillsAssociation: { x: number; y: number; occupation: { x: number; y: number; }; }[];
    associatedOccupations: Array<Occupation>;
    associatedOccupationsHexagonCoords: pointCoords[];
    selectedNode: Occupation = null;
    selectedOccupation: Occupation = null;
    skillNodes: { x: number; y: number; type: string }[];
    height: number;
    rotateCounter: number;
    createForm: any;
    graphJson: any;
    width: any;
    links: any;
    nodes: any;
    margin: { top: number; right: number; bottom: number; left: number; };
    showSkillButtons: { common: boolean; diff: boolean };

    @Input('occupation') occupation: Occupation;

    constructor(private occupationsService: OccupationsService, private skillsService: SkillsService, public platform: Platform) {
        this.associatedOccupations = [];
        this.graphJson = {};
        this.showSkillButtons = null;
        this.showSkillButtons = { common: false, diff: false };
    }

    showCommonSkills() {
        this.showSkillButtons.common = true;
        //this.skillsService.getCommonSkills(this.selectedNode.IRI, this.occupation.IRI).subscribe(skills => console.log(skills), err => console.log(err), () => {});
    }

    showDiffSkills() {
        this.showSkillButtons.diff = true;

    }

    goBack() {
        this.showSkillButtons.common = false;
        this.showSkillButtons.diff = false;
    }

    ngOnInit() {
        this.selectedOccupation = this.occupation;
        this.showSkillButtons = { common: false, diff: false };
    }

    ngOnChanges() {
        this.selectedOccupation = this.occupation;
        this.associatedOccupations = [];
        this.graphJson = {};
        d3.select("svg").selectAll("*").remove();
        this.occupation ?
            this.occupationsService
                .getAssociatedOccupations(this.occupation.IRI)
                .subscribe(
                    response => this.getAssociatedOccupationsSuccess(response),
                    error => this.getAssociatedOccupationsError(error),
                    () => { }
                ) : null;
    }

    getSelectedNodeDetails(selectedNode: Occupation) {
        this.occupationsService
            .getOccupationDetails(selectedNode.IRI)
            .subscribe(
                response => this.getSelectedNodeDetailsSuccess(response),
                error => this.getSelectedNodeDetailsError(error),
                () => { }
            );
    }

    getSelectedNodeDetailsSuccess(response) {
        this.selectedOccupation = response.occupation;
    }

    getSelectedNodeDetailsError(error) {
        console.log('Error: ', error);
    }

    initGraph() {
        this.svg = d3.select("svg");
        this.height = this.platform.width() < 400 ? this.platform.height() : this.platform.height() - 380;
        this.width = this.platform.width() < 960 ? this.platform.width() * 95 / 100 : this.platform.width() * 70 / 100;
        this.margin = { top: 20, right: 90, bottom: 30, left: 90 };
        this.hexagonTransitionFinished = true;
        this.svg.call(d3.zoom().on("zoom", () => {
            this.g.attr("transform", d3.event.transform)
        }))
        this.createOccupationAssociationsGraph();
    }

    generateHexagonMesh(nodes: Occupation[], centerPoint: pointCoords, diff, associatedHexagonRadius: number) {
        let hexCoords = [];

        let apothemPercentage: number = (Math.sqrt(3) / 2); //apothem of a hexagone is this number * radius 
        let radiusWithDiff = centerPoint.radius + diff;
        let apothemWithDiff = radiusWithDiff * apothemPercentage;

        let hexagonPositionsInMesh = [
            { x0: 2, y0: 0 },
            { x0: 1, y0: 1.5 },
            { x0: -1, y0: 1.5 },
            { x0: -2, y0: 0 },
            { x0: -1, y0: -1.5 },
            { x0: 1, y0: -1.5 },
        ];
        return nodes.map((node, index) => {
            return {
                x: centerPoint.x + hexagonPositionsInMesh[index].x0 * apothemWithDiff,
                y: centerPoint.y + hexagonPositionsInMesh[index].y0 * radiusWithDiff,
                radius: associatedHexagonRadius
            }
        })
    };

    generateBadgesMesh(associatedHexagonCenterCoords: pointCoords[]) {
        let apothemPercentage: number = (Math.sqrt(3) / 2); //apothem of a hexagone is this number * radius 

        let trapezePositionInMesh = [
            {
                pointLeft: {
                    x: 6 / 8,
                    y: 5 / 8
                },
                pointRight: {
                    x: 6 / 8,
                    y: -5 / 8
                },
                badge: {
                    x: 7 / 8,
                    y: 0
                }
            },
            {
                pointLeft: {
                    x: -1 / 4,
                    y: 7 / 8
                },
                pointRight: {
                    x: 1,
                    y: 1 / 4,
                },
                badge: {
                    x: 1 / 2,
                    y: 5 / 8
                }
            },
            {
                pointLeft: {
                    x: -1,
                    y: 1 / 4
                },
                pointRight: {
                    x: 1 / 4,
                    y: 7 / 8
                },
                badge: {
                    x: -1 / 2,
                    y: 5 / 8
                }
            },
            {
                pointLeft: {
                    x: -6 / 8,
                    y: -5 / 8
                },
                pointRight: {
                    x: -6 / 8,
                    y: 5 / 8
                },
                badge: {
                    x: -7 / 8,
                    y: 0
                }
            },
            {
                pointLeft: {
                    x: 1 / 4,
                    y: -7 / 8
                },
                pointRight: {
                    x: -1,
                    y: -1 / 4
                },
                badge: {
                    x: -1 / 2,
                    y: -5 / 8
                }
            },
            {
                pointLeft: {
                    x: 1,
                    y: -1 / 4
                },
                pointRight: {
                    x: -1 / 4,
                    y: -7 / 8
                },
                badge: {
                    x: 1 / 2,
                    y: -5 / 8
                }
            }
        ]

        return associatedHexagonCenterCoords.map((hexCoords, index) => {
            let apothem = apothemPercentage * hexCoords.radius;
            let hexagonSides = [
                { "x": hexCoords.radius * apothemPercentage + hexCoords.x, "y": -hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.radius * apothemPercentage + hexCoords.x, "y": hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.x, "y": hexCoords.y + hexCoords.radius },
                { "x": -hexCoords.radius * apothemPercentage + hexCoords.x, "y": hexCoords.radius / 2 + hexCoords.y },
                { "x": -hexCoords.radius * apothemPercentage + hexCoords.x, "y": -hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.x, "y": hexCoords.y - hexCoords.radius },
            ]

            return trapezePositionInMesh.map((sideCoords, index) => {
                let hexagoneBadge = {
                    trapeze: [
                        {
                            x: hexCoords.x + sideCoords.pointLeft.x * apothem,
                            y: hexCoords.y + sideCoords.pointLeft.y * hexCoords.radius
                        },
                        {
                            x: hexCoords.x + sideCoords.pointRight.x * apothem,
                            y: hexCoords.y + sideCoords.pointRight.y * hexCoords.radius
                        }
                    ],
                    badge: {
                        x: hexCoords.x + sideCoords.badge.x * apothem,
                        y: hexCoords.y + sideCoords.badge.y * hexCoords.radius
                    }
                }
                hexagoneBadge.trapeze.push(hexagonSides[index], hexagonSides[(index + 1) % 6])
                return hexagoneBadge;
            })
        })
    }

    rotateHexagons(numberOfRotations: number, callback) {
        if (this.hexagonTransitionFinished) {
            this.hexagonTransitionFinished = false;
            d3.selectAll(`.${this.classCommonToAllAssociatedOccupation}`)
                .transition()
                .duration(2000)
                .attr("transform", (d: Occupation) => {
                    let node = d3.select(`.${d.customClass.split(' ').join('.')}`)
                    let currentNodeIndex = parseInt(node.attr("index"));
                    let newIndex = (currentNodeIndex + numberOfRotations) % 6;

                    node.selectAll(`.badge-${currentNodeIndex}`).transition().delay(1000).style("fill", "none");
                    node.selectAll(`.badge.badge-${newIndex}, .trapeze.badge-${newIndex}`).transition().delay(1000).style("fill", this.badgeColor.trapeze);
                    node.selectAll(`.text.badge-${newIndex}`).transition().delay(1000).style("fill", this.badgeColor.text);

                    node.attr("index", () => { return newIndex });
                    let x = this.associatedOccupationsHexagonCoords[newIndex].x - parseInt(node.attr('coordsX'));
                    let y = this.associatedOccupationsHexagonCoords[newIndex].y - parseInt(node.attr('coordsY'));
                    node.attr('translateX', x);
                    node.attr('translateY', y);
                    return `translate(${x}, ${y})`;
                }).on("end", (node, index) => {
                    if (index == 5)
                        callback();
                })
        }
    }

    associationNodeClick(d) {
        //If only coords of the main occupation are needed then d3MainOccupation can be deleted and the coords can be stored into a variable outside the d3 scope
        let d3MainOccupation = d3.select('.mainOccupation');
        let clickedNode = d3.select(`.${d.customClass.split(' ').join('.')}`);
        let clickedNodeTranslate: any = {
            x: clickedNode.attr('translateX'),
            y: clickedNode.attr('translateY')
        }

        //If hexagon is the on the right side of the main occupation, on the same horizontal
        if (
            parseInt(d3MainOccupation.attr('coordsY')) == (parseInt(clickedNode.attr('coordsY')) + parseInt(clickedNode.attr('translateY'))) &&
            parseInt(d3MainOccupation.attr('coordsX')) < (parseInt(clickedNode.attr('coordsX')) + parseInt(clickedNode.attr('translateX'))) &&
            this.hexagonTransitionFinished
        ) {
            //Move hexagon and show relationship with skills
            clickedNode.attr("transform", (d: Occupation) => {
                d.selected = !d.selected;

                if (d.selected) {
                    this.selectedNode = d;
                    clickedNodeTranslate.x = parseInt(clickedNodeTranslate.x) + 200;
                    clickedNode.attr('translateX', clickedNodeTranslate.x);
                    this.showSkills(true);
                    this.getSelectedNodeDetails(this.selectedNode);
                    let d3SelectedNode = d3.select(`.${this.selectedNode.customClass.split(' ').join('.')}`);
                    this.skillNodeHexagonBadge(d3SelectedNode, false);
                    this.generateSkillNodeText(true);
                } else {
                    this.selectedOccupation = this.occupation;
                    let d3SelectedNode = d3.select(`.${this.selectedNode.customClass.split(' ').join('.')}`);
                    this.skillNodeHexagonBadge(d3SelectedNode, true);
                    this.generateSkillNodeText(false);
                    this.selectedNode = null;
                    clickedNodeTranslate.x = parseInt(clickedNodeTranslate.x) - 200;
                    clickedNode.attr('translateX', clickedNodeTranslate.x);
                    this.showSkills(false);
                }
                return `translate(${clickedNodeTranslate.x},${clickedNodeTranslate.y})`;
            });
        } else {
            if (this.selectedNode) {  //Translate back the selected node

                let d3SelectedNode = d3.select(`.${this.selectedNode.customClass.split(' ').join('.')}`)
                let d3SelectedNodeTranslate: any = {
                    x: d3SelectedNode.attr('translateX'),
                    y: d3SelectedNode.attr('translateY')
                }

                d3SelectedNode.attr("transform", (d: Occupation) => {
                    d.selected = false;
                    let d3SelectedNode = d3.select(`.${this.selectedNode.customClass.split(' ').join('.')}`);
                    this.skillNodeHexagonBadge(d3SelectedNode, true);
                    this.generateSkillNodeText(false);
                    this.selectedNode = null;
                    d3SelectedNodeTranslate.x = parseInt(d3SelectedNodeTranslate.x) - 200;
                    this.showSkills(false);
                    d3SelectedNode.attr('translateX', d3SelectedNodeTranslate.x);
                    return `translate(${d3SelectedNodeTranslate.x},${d3SelectedNodeTranslate.y})`;
                });
            }
            //Rotate Hexagons
            //Parameter: clicked node index in integer format
            //Callback is triggered after rotation ends
            this.rotateHexagons(6 - parseInt(clickedNode.attr("index")), () => {
                let clickedNodeTranslate: any = {
                    x: clickedNode.attr('translateX'),
                    y: clickedNode.attr('translateY')
                }
                clickedNode.attr("transform", (d: Occupation) => {
                    d.selected = true;
                    this.selectedNode = d;
                    this.getSelectedNodeDetails(this.selectedNode);
                    clickedNodeTranslate.x = parseInt(clickedNodeTranslate.x) + 200;
                    let d3SelectedNode = d3.select(`.${this.selectedNode.customClass.split(' ').join('.')}`);
                    this.showSkills(true);
                    this.generateSkillNodeText(true);
                    this.skillNodeHexagonBadge(d3SelectedNode, false);
                    clickedNode.attr('translateX', clickedNodeTranslate.x);
                    return `translate(${clickedNodeTranslate.x},${clickedNodeTranslate.y})`;
                });
                this.hexagonTransitionFinished = true;
            });
        }
    }

    skillNodeHexagonBadge(d3SelectedNode, shouldShow: boolean) {
        d3SelectedNode.selectAll(`.badge-0`).style("fill", "none");
        d3SelectedNode.selectAll(`.badge.badge-0, .trapeze.badge-0`).style("fill", shouldShow ? this.badgeColor.trapeze : "none");
        d3SelectedNode.selectAll(`.text.badge-0`).style("fill", shouldShow ? this.badgeColor.text : "none");
    }

    createOccupationAssociationsGraph() {
        //mainOccupation and customClasses associated
        this.occupation.customClass = "mainOccupation";
        let mainOccupationCoords: pointCoords = { x: 150, y: 200, radius: 70 };

        //associatedOccupation and customClasses associated
        this.classCommonToAllAssociatedOccupation = 'associated';
        this.associatedOccupations.forEach((occupation, index) => {
            occupation.customClass = this.classCommonToAllAssociatedOccupation + ' ' + 'node-' + index;
        });

        //console.log(this.occupation);

        let allOccupations = [this.occupation, ...this.associatedOccupations];

        //Calculate first the 6 associated hexagon centers by using mainHexagonRadius + diff as radius
        //After finding the centers use mainHexagonRadius and associatedHexagonRadius to design the hexagons with d3
        let gapBetweenMainAndAssociatedHexagons = 20;
        let associatedHexagonRadius = 80;

        this.associatedOccupationsHexagonCoords = this.generateHexagonMesh(this.associatedOccupations, mainOccupationCoords, gapBetweenMainAndAssociatedHexagons, associatedHexagonRadius);
        let allOccupationsHexagonCoords = [mainOccupationCoords, ...this.associatedOccupationsHexagonCoords]

        // this.svg.attr("transform","translate(" + this.margin.left + "," + this.margin.top + ")") 
        this.g = this.svg
            .append("g").attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
            .append("g").attr("transform", "translate(" + 103.47908500155617 + "," + 63.93163167161124 + ")scale(" + 0.6584848679816628 + ")");
        //Create lines for the given form
        this.createForm = d3.line()
            .x(function (d: any) { return d.x })
            .y(function (d: any) { return d.y })
            //.curve(d3.curveCardinalClosed.tension(0.55));
            .curve(d3.curveLinearClosed)

        //Having the center pont of a hex and a radius, generate the 6 intersection points
        let hexDataGenerator = (hexCoords: pointCoords) => {
            let apothemPercentage: number = (Math.sqrt(3) / 2);
            return [
                { "x": hexCoords.radius * apothemPercentage + hexCoords.x, "y": -hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.radius * apothemPercentage + hexCoords.x, "y": hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.x, "y": hexCoords.y + hexCoords.radius },
                { "x": -hexCoords.radius * apothemPercentage + hexCoords.x, "y": hexCoords.radius / 2 + hexCoords.y },
                { "x": -hexCoords.radius * apothemPercentage + hexCoords.x, "y": -hexCoords.radius / 2 + hexCoords.y },
                { "x": hexCoords.x, "y": hexCoords.y - hexCoords.radius },
            ];
        }

        let rotationDeg = 30;

        let occupationNodes = this.g.selectAll(".node")
            .data(allOccupations)
            .enter()
            .append("g")
            .attr("class", function (d: any) {
                return "node-occupation " + d.customClass;
            })

        this.g.selectAll(`.${this.classCommonToAllAssociatedOccupation}`).on("click", (d) => this.associationNodeClick(d));

        this.g.selectAll(".node-occupation")
            .data(allOccupationsHexagonCoords)
            .attr("translateX", 0)
            .attr("translateY", 0)
            .attr("index", function (d, i) { return i - 1; })
            .attr("coordsX", (coords: pointCoords) => { return coords.x })
            .attr("coordsY", (coords: pointCoords) => { return coords.y })
            .append("path")
            .attr("d", (coords: pointCoords) => { return this.createForm(hexDataGenerator(coords)) })
            .attr("stroke", "red")
            .attr("fill", "rgba(255,0,0,0.4)");

        this.createBadges();
        this.createText(allOccupations, mainOccupationCoords);

        let coordXOfClickedAssociatedOccupation = this.associatedOccupationsHexagonCoords[0].x + 200; //200 is the same as on the rotateHexagon click should be global
        let distanceYBetweenSkillNodes = 50; //define as global variable
        // let distanceXBetweenSkillAndOccupationNode

        let apothemPercentage: number = (Math.sqrt(3) / 2);
        this.occupationSkillsAssociation = [
            //{ x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y - distanceYBetweenSkillNodes, occupation: { x: mainOccupationCoords.x, y: mainOccupationCoords.y } },
            { x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y, occupation: { x: mainOccupationCoords.x + apothemPercentage * mainOccupationCoords.radius, y: mainOccupationCoords.y } },
            { x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y, occupation: { x: coordXOfClickedAssociatedOccupation - apothemPercentage * associatedHexagonRadius, y: mainOccupationCoords.y } },
            //{ x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y + distanceYBetweenSkillNodes, occupation: { x: coordXOfClickedAssociatedOccupation, y: mainOccupationCoords.y } },
        ]

        this.skillNodes = [
            //{ x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y + -distanceYBetweenSkillNodes, type: 'skill' },
            //{ x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y + distanceYBetweenSkillNodes, type: 'skill' },
            { x: (mainOccupationCoords.x + coordXOfClickedAssociatedOccupation) / 2, y: mainOccupationCoords.y, type: 'skill' },
        ];

        this.createLinksForSkillNodes(this.occupationSkillsAssociation);

        this.createSkillNodes(this.skillNodes);

        this.showSkills(false);
    }

    createBadges() {   
        this.badgeColor = {
            "trapeze": "gray",
            "badge": "gray",
            "text": "black"
        }
        let hexagonBadges = this.generateBadgesMesh(this.associatedOccupationsHexagonCoords);
        for (let index = 0; index < 6; index++) {
            let badgeIndex = (index + 3) % 6;
            d3.selectAll(`.${this.classCommonToAllAssociatedOccupation}`)
                .data(hexagonBadges)
                .append("path")
                .attr("d", (d: any) => { return this.createForm(d[index].trapeze) })
                .attr("index", badgeIndex)
                .attr("class", `trapeze badge-${badgeIndex}`)
                .style("fill", (d, i) => { return i == badgeIndex ? this.badgeColor.trapeze : "none"; })
            d3.selectAll(`.${this.classCommonToAllAssociatedOccupation}`)
                .data(hexagonBadges)
                .append("circle")
                .attr("cx", (d) => { return d[index].badge.x })
                .attr("cy", (d) => { return d[index].badge.y })
                .attr("r", 14)
                .attr("class", `badge badge-${badgeIndex}`)
                .style("fill", (d, i) => { return i == badgeIndex ? this.badgeColor.badge : "none"; })
            d3.selectAll(`.${this.classCommonToAllAssociatedOccupation}`)
                .data(hexagonBadges)
                .append("text")
                .attr("x", (d) => { return d[index].badge.x })
                .attr("y", (d) => { return d[index].badge.y + 2 })
                .attr("text-anchor", "middle")
                .text((d, i) => { return (Math.round(this.associatedOccupations[i].score * 100 / (this.occupation.skillsScore + this.associatedOccupations[i].skillsScore)) + "%") })
                .attr("font-family", "sans-serif")
                .attr("font-size", "10px")
                .attr("class", `text badge-${badgeIndex}`)
                .style("fill", (d, i) => { return i == badgeIndex ? this.badgeColor.text : "none"; })
        }
    }

    createText(allOccupations, mainOccupationCoords) {
        this.g.selectAll(".node-occupation")
            .data(allOccupations)
            .append("text")
            .attr("class", "node-occupation-text")
            .attr("x", (d: Occupation) => {
                let coordsX = d3.select(`.${d.customClass.split(' ').join('.')}`).attr('coordsX');
                return parseInt(coordsX) //- mainOccupationCoords.radius + 10;
            })
            .attr("y", (d: Occupation) => {
                return parseInt(d3.select(`.${d.customClass.split(' ').join('.')}`).attr('coordsY'));
            })
            .attr("text-anchor", "middle")
            .text((d: Occupation) => { return d.shortDescription })
            .attr("font-family", "sans-serif")
            .attr("font-size", "10px")
            .attr("fill", "black")
            .attr("inline-size", "10px");
        // let wrap = d3Wrap.textwrap()
        //     // wrap to 480 x 960 pixels
        //     .bounds({height: 80, width: 50})
        //     // // pad by an additional 10 pixels
        //     // .padding(10);
        // this.g.selectAll(".node-occupation-text")
        //     .call(wrap);
    }

    createLinksForSkillNodes(occupationSkillsAssociation) {
        this.g.selectAll(".link")
            .data(occupationSkillsAssociation)
            .enter().append("path")
            .attr("class", "link")
            .attr("d", function (d: any) {
                return d.occupation != null ? "M" + d.x + "," + d.y
                    + "C" + d.x + "," + (d.y + d.occupation.y) / 2
                    + " " + d.occupation.x + "," + (d.y + d.occupation.y) / 2
                    + " " + d.occupation.x + "," + d.occupation.y : null;
            });
    }

    createSkillNodes(skillNodes) {  
        this.g.selectAll(".skillNodes")
            .data(skillNodes)
            .enter()
            .append("g")
            .attr("class", "skillBadge")
            .attr("transform", function (d: any) {
                return "translate(" + d.x + "," + d.y + ")";
            })
            .append("circle")
                .attr("r", 30)
                .attr("class", "skillNodes")    

        this.g.selectAll(".skillBadge")
                .append("text")
                .attr("y", 2)
                .attr("class", "skillBadgeText")
                .attr("text-anchor", "middle")
                .attr("font-family", "sans-serif")
                .attr("font-size", "10px")
                .attr("fill", "black");
    }

    generateSkillNodeText(shouldShow: boolean) {
        if (shouldShow)
            this.g.selectAll(".skillBadgeText")
                .text(() => { return (Math.round(this.selectedNode.score * 100 / (this.occupation.skillsScore + this.selectedNode.skillsScore)) + "%") })
        else
            this.g.selectAll(".skillBadgeText")
                .text("")
    }

    showSkills(show: boolean) {
        this.g.selectAll(".skillNodes").transition().style("fill", show ? "grey" : "none");
        this.g.selectAll(".link").transition().style("stroke", show ? "grey" : "none");
    }

    getAssociatedOccupationsSuccess(response) {
        console.log(response);
        this.associatedOccupations = response.data;
        this.initGraph();
    }

    getAssociatedOccupationsError(error) {
    }

}