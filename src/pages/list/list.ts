import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { OccupationsService } from '../../services/OccupationsService';
import { SkillsService } from '../../services/SkillsService';
import { AssociationsService } from '../../services/AssociationsService';

import { Skill } from '../../interfaces/Skill';
import { Occupation } from '../../interfaces/Occupation';
import { Category } from '../../interfaces/Category';
import { OccupationAssociationsPage } from '../occupationAssociations/occupationAssociations';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
  providers: [OccupationsService]
})
export class ListPage {
  occupation: any;
  categories: Array<Category>;
  categoryTypes: Array<string>;
  occupations: Array<Occupation>
  codeISCOLike: string;
  searchInput : any;
  searchPlaceholder: string;
  occupationValidator: RegExp;
  inputIsNaN: boolean;

  ionViewWillEnter() {
    this.searchPlaceholder = 'Search by codeISCO or by Occupation Name';
    if (this.navParams.get('searchInput')) {
      this.searchInput = this.navParams.get('searchInput');
      setTimeout(() => {this.searchItems();}, 1000);
    } else {
      this.searchInput = '';
    }
    this.getOccupationCategories();
  }

  getOccupationCategories(categoryType = 0, codeISCOLike = '') {
    this.occupationsService
      .getOccupationCategories(this.categoryTypes[categoryType], codeISCOLike)
      .subscribe(
        response => this.occupationCategoriesSuccess(response),
        error => this.occupationsCategoriesError(error),
        () => {}
      )
  }

  getOccupationsByCodeISCO(codeISCO) {
    this.occupationsService
      .getOccupationByCodeISCO(codeISCO)
      .subscribe(
        response => this.getOccupationsSuccess(response),
        error => this.getOccupationsError(error),
        () => {}
      );
  }

  getOccupationsByName(occupationName) {
    this.occupationsService
      .getOccupationByName(occupationName)
      .subscribe(
        response => this.getOccupationsSuccess(response),
        error => this.getOccupationsError(error),
        () => {}
      );
  }

  constructor(public navCtrl: NavController,public navParams: NavParams,private occupationsService: OccupationsService,private alertCtrl: AlertController) {
    this.occupationValidator = new RegExp("^[a-zA-Z ]*$");
    this.categoryTypes = [
      'major',
      'subMajor',
      'minor',
      'unit',
      'occupations'
    ];
    this.searchInput = '';
    this.categories = [];
    this.occupations = [];
  }

  presentAlert(alertContent) {
    let alert = this.alertCtrl.create(alertContent);
    alert.present();
    //this.searchInput = '';
    //this.searchItems();
  }

  occupationsCategoriesError(error) {
  }
  occupationCategoriesSuccess(response) {
    this.occupations = [];
    this.categories = response.categories;
  }
  getOccupationsSuccess(response) {
    this.categories = [];
    this.occupations = response.occupations;
  }
  getOccupationsError(error) {
  }

  searchItems(event = null) {
    if (!isNaN(this.searchInput)) {
      this.inputIsNaN = false;
      if (this.searchInput.length < 4) {
        this.getOccupationCategories(this.searchInput.length, this.searchInput);
      } else {
        this.getOccupationsByCodeISCO(this.searchInput);
      }
    } else if (this.occupationValidator.test(this.searchInput)) {
      this.getOccupationsByName(this.searchInput);
      this.inputIsNaN = true;
    } else {
      this.presentAlert({title: 'Invalid search', subTitle: 'Please use only numbers and spaces for Occupations search or only Numbers for CodeISCO', buttons: ['Dismiss']});
      this.searchInput = '';
    }
  }

  categoryExtendByClick(categoryCodeISCO) {
    this.searchInput = categoryCodeISCO;
    this.searchItems();
  }

  occupationExtendByClick(occupation) {
    let searchInput = this.searchInput;
    this.navCtrl.setRoot(OccupationAssociationsPage, {
      occupation,
      searchInput
    });
  }

  goToGraphView(event) {
    this.navCtrl.setRoot(HomePage, {
      searchInput: this.searchInput
    });
    event.stopPropagation();
  }

}