import { Component } from '@angular/core';
import { OccupationsService } from '../../services/OccupationsService';

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
  providers: [OccupationsService]
})
export class DashboardPage {
  single: any[];
  singleHistogram: any[];
  multi: any[];
  view: any[]
  colorScheme: any;

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Number of common skills';
  showYAxisLabel = true;
  yAxisLabel = 'Number of associations';
  singlePieChart: any[];

  constructor(private occupationsService: OccupationsService) {
    this.view = [1200, 200];
    this.colorScheme = {
      domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    };
    this.single = [
      {
        "name": "Users",
        "value": 2
      },
      {
        "name": "Occupations",
        "value": 2950
      },
      {
        "name": "Associations",
        "value": 114921
      },
      {
        "name": "Occupation Associations",
        "value": 1018434
      },
      {
        "name": "Skills",
        "value": 13492
      },
      {
        "name": "Categories",
        "value": 500
      }
    ];
    this.singleHistogram = [];
    this.singlePieChart = [];

    this.occupationsService.getOccupationAssociationsHistogram()
      .subscribe(
        response => this.occupationAssociationsHistogramSuccess(response),
        error => this.occupationAssociationsHistogramError(error),
        () => {}
      )
  }

  occupationAssociationsHistogramSuccess(response) {
    this.singleHistogram = response.histogramArray.map(element => {
      return { name: element._id, value: Math.log(element.count) };
    });

    this.singlePieChart = [
      {"name": '1 Skill', "value": response.histogramArray[0].count},
      {"name": '2 Skills', "value": response.histogramArray[1].count},
      {"name": '3 Skills', "value": response.histogramArray[2].count},
      {"name": '4-6 Skills', "value": response.histogramArray.slice(3,5).reduce((sum, h) => sum + h.count, 0)},
      {"name": '7-15 Skills', "value": response.histogramArray.slice(6,14).reduce((sum, h) => sum + h.count, 0)},
      {"name": '16-50 Skills', "value": response.histogramArray.slice(15,49).reduce((sum, h) => sum + h.count, 0)},
      {"name": '50-118 Skills', "value": response.histogramArray.slice(50,response.histogramArray.length - 1).reduce((sum, h) => sum + h.count, 0)},
    ];
  }

  occupationAssociationsHistogramError(error) {
  }

  clickPie(event) {
  }

  onSelect(event) {
  }

}
