import { Component, ElementRef, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMapsProvider } from '../../providers/google-maps/google-maps';
import { GoogleMapsClusterProvider } from '../../providers/google-maps-cluster/google-maps-cluster';

@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html',
})
export class MapsPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  map: any;
  mapLoaded: any;
  mapWasInitialised: boolean;

  constructor(public platform: Platform, public maps: GoogleMapsProvider, public mapCluster: GoogleMapsClusterProvider) {
    this.mapWasInitialised = false;
  }

  ionViewWillEnter() {
    this.platform.ready().then(() => {
      this.mapLoaded = this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement).then((map) => {
        this.mapCluster.addCluster(map);
      })
    });
  }

  // ionViewDidLeave() {
  //   this.mapLoaded.remove();
  // }

}
