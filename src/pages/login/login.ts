import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AuthenticationService } from '../../services/AuthenticationService';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { DashboardPage } from '../dashboard/dashboard';
import { ListPage } from '../list/list';
import { OccupationAssociationsPage } from '../occupationAssociations/occupationAssociations';
import { HomePage } from '../home/home';
import { MapsPage } from '../maps/maps';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
    providers: [AuthenticationService]
})
export class LoginPage {
    private loginForm: FormGroup;
    loginInProgress: any;

    constructor(
        private formBuilder: FormBuilder,
        public navCtrl: NavController,
        private authenticationService: AuthenticationService,
        private storage: Storage,
        private alertCtrl: AlertController
    ) {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
        });
        this.loginInProgress = false;
        // this.guestLogin();
    }

    guestLogin() {
        if (!this.loginInProgress) {
            this.authenticationService.post({ username: 'alexandrup', password: 'test' }).subscribe(
                response => this.loginSuccess(response),
                error => this.loginError(error),
                () => { }
            );
            this.loginInProgress = true;
        }
    }

    signIn() {
        if (!this.loginInProgress) {
            this.authenticationService.post(this.loginForm.value).subscribe(
                response => this.loginSuccess(response),
                error => this.loginError(error),
                () => { }
            );
            this.loginInProgress = true;
        }
    }

    loginSuccess(response) {
        this.loginInProgress = false;
        if (response.success) {
            localStorage.setItem('token', response.token.toString());
            this.storage.set('token', response.token);
            // this.navCtrl.setRoot(HomePage);
            this.navCtrl.setRoot(ListPage);
        } else {
            let alert = this.alertCtrl.create({
                "title": "Error",
                "subTitle": response.message,
                "buttons": [
                    {
                        text: 'Ok',
                        role: 'Ok',
                        handler: () => {
                            this.loginForm.reset();
                        }
                    },
                ]
            })
            alert.present();
        }
    }

    loginError(error) {
    }

}
